// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// http://localhost:7200/api/
// https://api.tapesmusic.co.uk/api/

export const environment = {
  production: false,
  apiUrl: 'https://api.tapesmusic.co.uk/api/',
  stripeKey: 'pk_test_nc8qzGfii1lDh8SVKDytFvKK',
  firebase: {
    apiKey: 'AIzaSyDzrLEcDWzp-dk8nu3p_pbpJMPjBUhANlU',
    authDomain: 'legacycoe001.firebaseapp.com',
    databaseURL: 'https://legacycoe001.firebaseio.com',
    projectId: 'legacycoe001',
    storageBucket: 'legacycoe001.appspot.com',
    messagingSenderId: '989981351038',
    appId: '1:989981351038:web:d92f1ca51d881ce1260c2f',
    measurementId: 'G-QNR4G79D99'
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
