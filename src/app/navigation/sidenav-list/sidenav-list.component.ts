import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
 
@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();
 
  constructor
  (
    private userService: UserService,
    private router: Router
  ) { }
 
  ngOnInit() {
  }

  logout() {
    this.userService.logout();
    this.router.navigate(["/user/login"]);
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }
 
}
