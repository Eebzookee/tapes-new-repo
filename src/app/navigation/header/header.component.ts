import { Component, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { Location } from "@angular/common";
import { HeaderService } from './header.service';
import { AudioService } from '../../services/audio/audio.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit,  AfterViewInit {
  pagetitle: string = 'Tapes';
  pagetitle2: string = '';
  pagetype: string = 'withmenu';
  pageloading: string = 'false';
  track:any = null

  @Output() public sidenavToggle = new EventEmitter();
  
  constructor(
    private headerService: HeaderService,
    private audioService: AudioService,
    private _location: Location,
    ) { }

  ngOnInit(): void {
  }
  
  ngAfterViewInit() {
    this.headerService.subject.subscribe((pagetype: any) => {
      this.pagetype = pagetype;
    });
    this.headerService.subjectTitle.subscribe((_pagetitle: any) => {
      const x = _pagetitle.split('|')
      this.pagetitle = x[0];
      this.pagetitle2 = x[1];
      console.log('pagetitle:', _pagetitle, this.pagetitle2)
    });
    this.headerService.subjectLoading.subscribe((pageloading: any) => {
      this.pageloading = pageloading;
    });
    this.audioService.subjectTrack.subscribe((track: any) => {
      console.log('***track', track);
      this.track = track;
    });
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  backClicked() {
    this._location.back();
  }

  trackEvent($event:Event) {
    console.log('$event', $event)
    //this.track = undefined;
    //this.trackdata.playCurrentTrack(this.track);
}

}
