import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  private _pagetype: string = 'withmenu';
  private _pagetitle: string = 'Tapes';
  private _pageloading: string = 'false';
  subject = new Subject();
  subjectTitle = new Subject();
  subjectLoading = new Subject();

  changePageType(pagetype: string) {
    this._pagetype = pagetype;
    this.subject.next(this._pagetype);
  }

  changePageTitle(pagetitle: string) {
    this._pagetitle = pagetitle;
    this.subjectTitle.next(this._pagetitle);
  }

  changePageLoading(pageloading: string) {
    this._pageloading = pageloading;
    this.subjectLoading.next(this._pageloading);
  }

  clear() {
    this._pagetype = 'withmenu';
    this.subject.next(this._pagetype);

    this._pagetitle = 'Tapes';
    this.subjectTitle.next(this._pagetitle);

    this._pageloading = 'false';
    this.subjectLoading.next(this._pageloading);

  }
}
