import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../navigation/header/header.service';
import {AudioService} from '../services/audio/audio.service';
import {Track} from '../models/track/track.model';
import {SortByOptions} from '../services/helpers';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  loading = false;
  genres: { name: string }[] = [];
  selectedGenre: { name: string } | null = null;
  searchField = '';
  filteredTracks: Track[] = [];
  sortByOptions: {label: string, value: SortByOptions}[] = [
    {
      label: 'Likes',
      value: SortByOptions.LIKES
    },
    {
      label: 'Downloads',
      value: SortByOptions.DOWNLOADS
    },
    {
      label: 'Plays',
      value: SortByOptions.PLAYS
    },
    {
      label: 'Comments',
      value: SortByOptions.COMMENTS
    }
  ];
  selectedSortByOption = SortByOptions.LIKES;

  constructor(
    private headerService: HeaderService,
    private audioService: AudioService
  ) { }

  ngOnInit(): void {
    this.headerService.changePageTitle('|Search');

    this.audioService.getAllGenres().subscribe(response => {
      this.genres = response;
    });
  }

  genreOnClick(genre: {name: string}) {
    if (this.selectedGenre === genre) {
      this.selectedGenre = null;
    } else {
      this.selectedGenre = genre;
    }

    this.filter();
  }

  filter() {
    if (!this.selectedGenre && !this.searchField) {
      return;
    }

    this.loading = true;

    this.audioService.getTracks({
      filter: this.searchField === '' ? undefined : {
        title: this.searchField,
        artist: this.searchField
      },
      genre: this.selectedGenre ? this.selectedGenre.name : undefined
    }).subscribe(response => {
      this.filteredTracks = response.sort((a, b) => {
        switch (this.selectedSortByOption) {
          case SortByOptions.PLAYS:
            return b.metrics.numberPlays.value - a.metrics.numberPlays.value;
          case SortByOptions.COMMENTS:
            return b.metrics.numberComment.value - a.metrics.numberComment.value;
          case SortByOptions.DOWNLOADS:
            return b.metrics.numberDownloads.value - a.metrics.numberDownloads.value;
          case SortByOptions.LIKES:
            return b.metrics.numberLikes.value - a.metrics.numberLikes.value;
        }
      });

      this.loading = false;
    }, error => {
      console.log(error);
      this.loading = false;
    });
  }
}
