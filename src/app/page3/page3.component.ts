import { Component, OnInit, AfterViewInit } from '@angular/core';
import {Location} from '@angular/common';
import { HeaderService } from "../navigation/header/header.service";

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.scss']
})
export class Page3Component implements OnInit {
  

  constructor(
    private headerService: HeaderService,
    private _location: Location
  ) {}

  ngOnInit(): void {
    this.headerService.changePageType("withmenu");
    this.headerService.changePageTitle("page 3");
  }

  ngAfterViewInit() {

  }

  backClicked() {
    this._location.back();
  }

}
