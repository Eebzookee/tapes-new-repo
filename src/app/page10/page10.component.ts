import { Component, OnInit, AfterViewInit } from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { speshCharValidator } from "../validators/custom.validators";
import { Location } from "@angular/common";
import { HeaderService } from "../navigation/header/header.service";

@Component({
  selector: 'app-page10',
  templateUrl: './page10.component.html',
  styleUrls: ['./page10.component.scss']
})
export class Page10Component implements OnInit {
  formGroup!: FormGroup;
  inviteCode!: string;


  constructor(
    private headerService: HeaderService,
    private _location: Location,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.headerService.changePageType("withnomenu");
    this.headerService.changePageTitle("page 10");    


    this.formGroup = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      city: ["", Validators.required],
      instagram: ["", Validators.required],
      web: [""],
      twitter: [""],
      profilePicture: ["", Validators.required],
      bio: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(8), speshCharValidator]],
      passwordRepeat: ["", Validators.required],
      genres: [[]],
      podcasts: [],
      radioStations: [],
      other: [],
      clubs: []
    });


  }

  backClicked() {
    this._location.back();
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.formGroup.controls[controlName].hasError(errorName);
  }


  yourDetailsCompleted() {
    return this.formGroup.controls["name"].valid &&
      this.formGroup.controls["email"].valid &&
      this.formGroup.controls["city"].valid &&
      this.formGroup.controls["instagram"].valid;
  }

  yourGenresCompleted() {
    return this.formGroup.controls["genres"].valid;
  }

  yourProfileCompleted() {
    return this.formGroup.controls["bio"].valid &&
      this.formGroup.controls["profilePicture"].valid;
  }

  passwordCompleted() {
    return this.formGroup.controls["password"].valid &&
      this.formGroup.controls["passwordRepeat"].valid &&
      this.formGroup.controls["password"].value === this.formGroup.controls["passwordRepeat"].value;
  }

  whereYouPlayCompleted() {
    return this.formGroup.controls["other"].value.length > 0 ||
      this.formGroup.controls["radioStations"].value.length > 0 ||
      this.formGroup.controls["podcasts"].value.length > 0 ||
      this.formGroup.controls["clubs"].value.length > 0
  }

  genresCompleted() {
    return this.formGroup.controls["genres"].value.length > 0;
  }



  async completeSignup() {
    console.log('completeSignup');
    /*
    this.loadingService.setLoading(true);

    let formData = this.formGroup.value;
    formData["type"] = "DJ";
    formData["inviteCode"] = this.inviteCode;

    this.signupRepository.register(formData).subscribe(response => {
      if (response.error) {
        this.notifyService.error(response.error);
      } else {
        this.notifyService.success("message.success");
        this.router.navigateByUrl("/");
        this.authService.openAuthDialog("login");
      }

      this.loadingService.setLoading(false);
    });
    */
  }

}
