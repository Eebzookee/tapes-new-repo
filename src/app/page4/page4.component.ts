import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Location } from "@angular/common";
import { HeaderService } from "../navigation/header/header.service";
@Component({
  selector: "app-page4",
  templateUrl: "./page4.component.html",
  styleUrls: ["./page4.component.scss"],
})
export class Page4Component implements OnInit, AfterViewInit {
  constructor(
    private headerService: HeaderService,
    private _location: Location
  ) {}

  ngOnInit(): void {
    this.headerService.changePageType("withmenu");
    this.headerService.changePageTitle("page 4");    
  }

  ngAfterViewInit() {

  }

  backClicked() {
    this._location.back();
  }
}
