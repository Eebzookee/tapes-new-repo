import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {User} from '../../models/user/user.model';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.scss']
})
export class ViewUsersComponent implements OnInit {

  users: User[] = [];
  displayedColumns: string[] = ['id', 'name', 'type'];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getAll().subscribe(response => {
      this.users = response;
    }, error => {
      console.log(error);
    });
  }

}
