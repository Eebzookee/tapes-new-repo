import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { HeaderService } from "../navigation/header/header.service";

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class Page2Component implements OnInit {

  constructor(
    private headerService: HeaderService,
    private _location: Location
  ) {}

  ngOnInit(): void {
    this.headerService.changePageType("withmenu");
    this.headerService.changePageTitle("page 2");
  }

  backClicked() {
    this._location.back();
  }

}
