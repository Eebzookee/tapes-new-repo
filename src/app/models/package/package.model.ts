export class Package {  
    constructor(
      public id: number,
      public name: string,
      public sub_price: number,
      public track_limit: number,
      public description: string,
    ) {
    }
  }

  const PackageMapper = (data: any): Package => {
    return new Package(
      data?.id,
      data?.name,
      data?.sub_price,
      data?.track_limit,
      data?.description,
    );
  };

/*
  export class PackageMapper {
    static map(data: any): Package {
      return new Package(
        data?.id,
        data?.name,
        data?.sub_price,
        data?.track_limit,
        data?.description,
      );
    }
  }
*/

  export {PackageMapper};