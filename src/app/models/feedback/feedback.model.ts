import {Track, TrackMapper} from '../track/track.model';

export class Feedback {
  constructor(
    public id: number,
    public comment: string,
    public date: string,
    public trackId: number,
    public userId: number,
    public commentId: number,
    public track: Track,
    public givenToken: boolean
  ) {
  }
}

export const FeedbackMapper = (data: any): Feedback => {
  return {
    id: data?.id,
    comment: data?.comment,
    date: data?.date,
    trackId: data?.track_id,
    userId: data?.user_id,
    commentId: data?.comment_id,
    track: TrackMapper.map(data?.track),
    givenToken: data?.given_token === 1
  };
};

export interface TrackComment {
  id: number;
  trackId: number;
  userId: number;
  comment: string;
  date: Date;
  userName: string;
}

export const TrackCommentMapper = (data: any): TrackComment => {
  const date = new Date(data?.date);

  return {
    id: data?.id,
    trackId: data?.track_id,
    userId: data?.user_id,
    comment: data?.comment,
    date,
    userName: data?.name
  };
};
