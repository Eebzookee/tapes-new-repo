import {User, UserMapper} from '../user/user.model';

export class Metric {
  constructor(
    public value: number,
    public change: number
  ) {
  }
}


export class Metrics {
  constructor(
    public numberLikes: Metric,
    public numberDownloads: Metric,
    public numberPlays: Metric,
    public numberShares: Metric,
    public numberComment: Metric
  ) {
  }
}

export class MetricsMapper {
  public static map(data: any): Metrics {
    return new Metrics(
      new Metric(data.num_likes || 0, 0), // number of likes
      new Metric(data.num_downloads || 0, 0), // number of downloads
      new Metric(data.num_plays || 0, 0), // number of plays
      new Metric(data.num_shares || 0, 0), // number of shares
      new Metric(data.num_comments || 0, 0) // number of comments
    );
  }
}


export interface TrackVersion {
  id: number;
  name: string;
  url: string;
  duration: string;
}

const TrackVersionMapper = (data: any): TrackVersion => {
  return {
    id: data.id || -1,
    name: data.name || '',
    url: data.url || '',
    duration: data.duration || ''
  };
};

export class Track {
  public selectedVersion: number;

  constructor(
    public id: number,
    public title: string,
    public cover: string,
    public duration: number,
    public audio: string,
    public genre: string,
    public date: string,
    public description: string,
    public user: User,
    public liked: boolean,
    public metrics: Metrics,
    public inactive: boolean,
    public featured: boolean,
    public versions: TrackVersion[]
  ) {
    // Temp TODO
    this.liked = true;
    this.selectedVersion = versions[0] ? versions[0].id : -1; // TODO check there is at least one version here
  }
}

// TODO: convert to arrow function
export class TrackMapper {
  static map(data: any): Track {
    return new Track(
      data?.id,
      data?.title,
      data?.cover,
      data?.duration,
      data?.audio,
      data?.genre,
      data?.date,
      data?.description,
      UserMapper(data.user),
      data?.liked,
      MetricsMapper.map(data?.metrics),
      data?.inactive === 1,
      data?.featured === 1,
      data?.versions.map(TrackVersionMapper)
    );
  }
}
