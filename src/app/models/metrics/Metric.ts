export class Metric {
  constructor
  (
    public value: number,
    public change: number
  ) {
  }
}
