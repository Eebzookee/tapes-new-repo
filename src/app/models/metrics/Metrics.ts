import {Metric} from "./Metric";

export class Metrics {
  constructor
  (
    public numberLikes: Metric,
    public numberDownloads: Metric,
    public numberPlays: Metric,
    public numberShares: Metric,
    public numberComment: Metric
  ) {
  }
}

export class MetricsMapper {
  public static map(data: any): Metrics {
    return new Metrics(
      new Metric(data["num_likes"] || 0, 0), // number of likes
      new Metric(data["num_downloads"] || 0, 0), // number of downloads
      new Metric(data["num_plays"] || 0, 0), // number of plays
      new Metric(data["num_shares"] || 0, 0), // number of shares
      new Metric(data["num_comments"] || 0, 0) // number of comments
    );
  }
}
