export class User {
    constructor(
        public id: number,
        public name: string,
        public email: string,
        public avatar: string,
        public bio: string,
        public city: string,
        public type: string,
        public created: string,
        public genres: string[],
        public role: string
    ) {}
}

const UserMapper = (data: any): User => {
  return new User(
    data?.id,
    data?.name,
    data?.email,
    data?.avatar,
    data?.bio,
    data?.city,
    data?.type,
    data?.created,
    data?.genres.split(', '),
    data?.role
  );
};

export {UserMapper};
