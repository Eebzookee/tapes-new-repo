import { Track, TrackMapper } from "../track/track.model";

export class Playlist {
    constructor(
        public id: number,
        public name: string,
        public tracks: Track[]
    ) {};
}

export const PlaylistMapper = (data: any): Playlist => {
    return {
        id: data?.id,
        name: data?.name,
        tracks: data?.tracks.map((t: any) => TrackMapper.map(t))
    }
}