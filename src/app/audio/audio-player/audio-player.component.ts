import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  Inject
} from "@angular/core";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AudioPlayerConfig } from "../audio-player-config";
import { AudioService } from "../../services/audio/audio.service";
//import { AuthService } from 'app/core/auth.service';
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs";
//import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
//import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable, fromEvent, merge } from "rxjs";
import { tap } from "rxjs/operators";
//import * as firebase from 'firebase/app';
//import 'rxjs/add/observable/fromEvent';
//import 'rxjs/add/observable/merge';
//import 'rxjs/add/operator/delay';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/startWith';
import { map, delay } from "rxjs/operators";

@Component({
  selector: "app-audio-player",
  templateUrl: "./audio-player.component.html",
  styleUrls: ["./audio-player.component.scss"],
})
export class AudioPlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() audioSrc!: string;
  @Input() config!: AudioPlayerConfig;
  @Input() track: any;
  @Output() trackEvent = new EventEmitter<string>();
  @ViewChild("audioPlayer") audioPlayer!: ElementRef;
  @ViewChild("playhead") playhead!: ElementRef;
  @ViewChild("timeline") timeline!: ElementRef;
  public trackLength$!: Observable<number>;
  public isPlaying: boolean = false;
  public wasPlaying: boolean = false;
  public isReadyForPlayback = false;
  public currentTimeDisplay$!: Observable<number>;
  public playheadPosition$!: Observable<number>;
  public user: any;
  private eventCounter: number = 0;
  mousedownIsOnPlayhead: boolean = false;
  private init_playheadPosition: any;
  favorite: string = "favorite_border";
  djliked: boolean = false;
  download: string = "cloud_download";
  toggleStatus: string = "fullscreen_exit";
  isFullscreen: boolean = false;
  //private track_collection: AngularFirestoreCollection < any > = this.db.collection('track');
  //private track_obs = this.track_collection.valueChanges();
  //private user_collection: AngularFirestoreCollection < any > = this.db.collection('user');
  //private user_obs = this.user_collection.valueChanges();
  //private ip;
  profile: any;

  hs_sub1: any;
  hs_sub2: any;
  hs_sub3: any;
  // Private
  private _unsubscribeAll!: Subject<any>;

  constructor(private http: HttpClient, private audioService: AudioService, public dialog: MatDialog) {
    /*
        this.hs_sub3 = this.authService.currentUserafs.subscribe(user => {
          this.user = user || [];
          // console.log('this.user', this.user);

          // look to see if its a favorite
          ('liked' in this.track) ? '' : this.track.liked = []; // inilise liked property for this track
          const pos = this.track.liked.indexOf(this.user.uid);
          if (~pos) {
              this.favorite = 'favorite';
              console.log('this user favorited this track')
              const allowedRoles = ['editor'];
              for (const role of allowedRoles) {
                  if (this.user.roles[role]) {
                      console.log('dj liked this');
                      this.djliked = true;
                  }
              }
          }
      });
      */
  }



  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    if (this.hs_sub1 && !this.hs_sub1.closed) this.hs_sub1.unsubscribe();
    if (this.hs_sub2 && !this.hs_sub2.closed) this.hs_sub2.unsubscribe();
    if (this.hs_sub3 && !this.hs_sub3.closed) this.hs_sub3.unsubscribe();

    if (this._unsubscribeAll && !this._unsubscribeAll.closed) {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    }
    this.track = undefined;
    //console.log('goooone');
  }

  ngAfterViewInit(): void {}

  ngOnInit() {
    this.setOnLoadConfig();
    setTimeout(() => {
      this.toggleConfig();
      this.trackLength$ = fromEvent(
        this.audioPlayer.nativeElement,
        "canplaythrough"
      )
        .pipe(map(() => this.setTrackLengthOnLoad()))
        .pipe(delay(10));
      this.currentTimeDisplay$ = fromEvent(
        this.audioPlayer.nativeElement,
        "timeupdate"
      )
        .pipe(map(() => this.updateCurrentTimeDisplay()))
        .pipe(delay(0));
      const playheadPositionTimelineClick$ = fromEvent(
        this.timeline.nativeElement,
        "click"
      )
        .pipe(map((ev: any) => this.manuallyMovePlayhead(ev)))
        .pipe(delay(0));
      const playheadPositionTimeupdate$ = fromEvent(
        this.audioPlayer.nativeElement,
        "timeupdate"
      )
        .pipe(map(() => this.updatePlayheadPosition()))
        .pipe(delay(0));
      const playheadMousemove$ = fromEvent(document, "mousemove")
        .pipe(
          map((mmEv: any) => {
            if (this.mousedownIsOnPlayhead) {
              const newPosition = this.manuallyMovePlayhead(mmEv);
              return newPosition;
            }
            return (
              100 *
              (this.audioPlayer.nativeElement.currentTime /
                this.audioPlayer.nativeElement.duration)
            );
          })
        )
        .pipe(delay(0));

      this.init_playheadPosition = merge(
        playheadPositionTimeupdate$,
        playheadMousemove$,
        playheadPositionTimelineClick$
      );
      this.audioSrc = this.track.versions[0].url;
      

      setTimeout(() => {
        this.playheadPosition$ = this.init_playheadPosition;
      }, 5000);

      this.playhead.nativeElement.addEventListener(
        "mousedown",
        this.onPlayheadMousedownEvent.bind(this)
      );
      document.addEventListener(
        "mouseup",
        this.onPlayheadMouseupEvent.bind(this)
      );
    }, 100);
  }

  ngOnChanges() {
    if (this.audioSrc) {
      this.reset();
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: this.track.versions
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) this.audioSrc = result.data.url;
    });
  }


  onPlayheadMousedownEvent() {
    this.mousedownIsOnPlayhead = true;
    this.wasPlaying = !!this.isPlaying;
    this.pause();
  }

  onPlayheadMouseupEvent() {
    this.mousedownIsOnPlayhead = false;
    if (this.wasPlaying) {
      this.play();
    }
    this.wasPlaying = false;
  }

  setOnLoadConfig() {
    console.log("this.config", this.config);

    this.config = {
      timelineConfig: {
        timelineColor: "#333333",
        timelineHeight: 4,
        playheadHeight: 12,
        playheadColor: "#8D5EEE",
        opacity: "0.6",
      },
      playPauseConfig: {
        iconClassPrefix: "fa",
        iconPixelSize: 60,
        pauseIconClass: "fa-pause",
        pauseIconColor: "#ff0",
        playIconClass: "fa-play",
        playIconColor: "#8D5EEE",
      },
    };
    console.log("this.config", this.config);
  }

  toggleConfig() {
    this.config = {
      timelineConfig: {
        timelineColor: "#333333",
        timelineHeight: 4,
        playheadHeight: 12,
        playheadColor: "#8D5EEE",
        opacity: "0.6",
      },
      playPauseConfig: {
        iconClassPrefix: "fa",
        iconPixelSize: 26,
        pauseIconClass: "fa-pause",
        pauseIconColor: "#8D5EEE",
        playIconClass: "fa-play",
        playIconColor: "#8D5EEE",
      },
    };
  }

  setTrackLengthOnLoad(): number {
    this.isReadyForPlayback = true;
    return Math.ceil(this.audioPlayer.nativeElement.duration);
  }

  updateCurrentTimeDisplay(): number {
    const current = this.audioPlayer.nativeElement.currentTime;
    const duration = this.audioPlayer.nativeElement.duration;
    if (current === duration) {
      return 0;
    }
    return Math.ceil(current);
  }

  updatePlayheadPosition(): number {
    const percentAsDecimal =
      this.audioPlayer.nativeElement.currentTime /
      this.audioPlayer.nativeElement.duration;
    if (percentAsDecimal >= 1) {
      this.isPlaying = false;
      return 0;
    }
    return 100 * percentAsDecimal;
  }

  manuallyMovePlayhead(ev: any) {
    let newPercent = this.getPercentPosition(ev);
    newPercent = newPercent < 0 ? 0 : newPercent;
    newPercent = newPercent > 1 ? 1 : newPercent;
    if (newPercent < 1) {
      const newCurrentTime: number =
        this.audioPlayer.nativeElement.duration * newPercent;
      this.audioPlayer.nativeElement.currentTime = newCurrentTime;
      return 100 * newPercent;
    } else if (newPercent === 1) {
      return 100;
    }
    return 0;
  }

  getPercentPosition(ev: any) {
    const clientX: number = ev.clientX;
    const boundingClientRectLeft: number = this.timeline.nativeElement.getBoundingClientRect()
      .left;
    const timelineWidth: number = this.timeline.nativeElement.offsetWidth;
    const newPercent = (clientX - boundingClientRectLeft) / timelineWidth;
    return newPercent;
  }

  play() {
    if (!this.isPlaying) {
      this.audioPlayer.nativeElement.play();
      this.isPlaying = true;
      this.played();
    }
  }

  pause() {
    if (this.isPlaying) {
      this.audioPlayer.nativeElement.pause();
      this.isPlaying = false;
    }
  }

  reset() {
    this.isPlaying = false;
    this.wasPlaying = false;
  }

  closePlayer() {
    this.trackEvent.emit();
    this.audioPlayer.nativeElement.pause();
    //this.track = undefined;
    this.audioService.changeTrack(null);
  }

  likethis(track: any) {
    "liked" in track ? "" : (track.liked = []);
    const pos = track.liked.indexOf(this.user.uid);
    if (this.favorite === "favorite") {
      this.favorite = "favorite_border";
      if (~pos) track.liked.splice(pos, 1);
    } else {
      this.favorite = "favorite";
      if (!~pos) track.liked.push(this.user.uid);
    }
    //save track document
    //this.track_collection.doc(track.id).update(track);
    // if the user is a dj save it to that profile
    // get the user and add uid of track to usrs favorites list
    /*
      this.hs_sub1 = this.authService.currentUserafs.subscribe(profile => {
          this.profile = profile || [];
          //console.log('is this user a dj', profile);
          
          //this.profile = new Profile(profile);
          //this.profileForm = this.createProfileForm();
          //this.checkartistprofile(profile);
      });
      */
  }

  togglePlayer() {
    if (this.toggleStatus === "fullscreen") {
      this.toggleStatus = "fullscreen_exit";

      this.setOnLoadConfig();

      this.isFullscreen = false;
    } else {
      this.toggleStatus = "fullscreen";
      this.toggleConfig();
      this.isFullscreen = true;
    }
    //console.log('this.isFullscreen:', this.isFullscreen);
  }

  getip() {
    return this.http.jsonp("https://api.ipify.org?format=jsonp", "callback");
  }

  played() {
    /*
      this.hs_sub2 = this.getip().subscribe(
        
          (res) => {
              this.ip = res;
              
              if('played' in this.track){
                  console.log('already exist?', this.track);
                  // do nothing
              } else {
                  console.log('create array');
                  this.track.played = [];
              }
              
              this.track.played.push({ ip: this.ip.ip, userid: this.user.uid, timestamp: Date.now() }); // || this.track.played === [];
              let playedList = this.track.played.filter((played:any) => played.userid === this.user.uid); //get playedlist for this user
              let lastPlay;
              if (playedList.length > 1) {
                  playedList = playedList.sort(function(obj1:any, obj2:any) {
                      return obj1.timestamp - obj2.timestamp; // sort this users plays
                  });
                  lastPlay = playedList[playedList.length - 2]; // compare to last entered before this current one                        
              } else {
                  lastPlay = playedList[playedList.length - 1]
              }

              // todo make this arbitrary 5 minute wiat be the actual length of the track in question. this should be easy to find out how many milliseconds it is
              (((Date.now() - lastPlay.timestamp) > 300000) || (playedList.length === 1)) ? // track hasn't been played by this user in the last 5 minutes   
              //this.track_collection.doc(this.track.id).update(this.track): ''
          }
      );
      */
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  changeTrack(version:any) {
    console.log('version', version);
    this.dialogRef.close({ data: version })
  }

}