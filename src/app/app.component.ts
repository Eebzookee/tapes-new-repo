import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { UserService } from './services/user/user.service';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'ang-material-owneraccount';
  mySubscription:any

  constructor
  (
    private userService: UserService,
    private permissionsService: NgxPermissionsService,
    private router: Router, 
    private activatedRoute: ActivatedRoute
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
         // Trick the Router into believing it's last link wasn't previously loaded
         this.router.navigated = false;
      }
    }); 
  }

  ngOnInit(): void {
    this.permissionsService.permissions$.subscribe(p => {
      console.log(p)
    })
    window.scroll(0,0);
  }

  ngOnDestroy(){
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

  scrollTop(event:Event) {
    window.scroll(0,0);
  }

}
