import { Injectable, OnDestroy, } from '@angular/core';
import Axios from 'axios';
import {Track, TrackMapper} from '../../models/track/track.model';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {handleError, TrackSearchOptions} from '../helpers';
import { Subject } from 'rxjs';
import {TrackCommentMapper, TrackComment} from '../../models/feedback/feedback.model';

@Injectable({
  providedIn: 'root'
})
export class AudioService implements OnDestroy {
  private _track: any = {};
  subjectTrack = new Subject();
  private _unsubscribeAll!: Subject<any>;
  constructor(
    private http: HttpClient,
  ) { }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    if (this._unsubscribeAll && !this._unsubscribeAll.closed) {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
    }
  }

  download(versionId: number): Observable<any> {
    return this.http.get(
      environment.apiUrl + 'audio/download/' + versionId, {responseType: 'blob'}
    );
  }

  addComment(track: number, comment: string) {
    return this.http.post(environment.apiUrl + 'audio/addComment', {
      trackId: track,
      comment
    }).pipe(catchError(handleError));
  }

  getTrackComments(track: number): Observable<TrackComment[]> {
    return this.http.get<TrackComment[]>(environment.apiUrl + 'audio/getComments/' + track).pipe(
      map(data => data.map(TrackCommentMapper))).pipe(
      catchError(handleError));
  }

  toggleLike(track: number) {
    return this.http.post(environment.apiUrl + 'audio/toggleLike/' + track, {}).pipe(catchError(handleError));
  }

  uploadTrack(file: File, onProgress: (progress: number) => void) {
    const formData = new FormData();
    formData.append('file', file);

    Axios.post('https://api.tapesmusic.co.uk/api/audio/uploadAudioFile', formData, {
      headers: {
        Authorization: localStorage.getItem('token')
      },
      onUploadProgress: (progress => onProgress(progress.loaded / progress.total * 100))
    }).then(response => console.log(response)).catch(error => console.log(error));
  }

  async upload(data: {
    title: string,
    description: string,
    genre: string,
    cover: string
  },           versions: {
    file: File,
    name: string
  }[],         onProgress: (progress: number) => void): Promise<{success: boolean, error?: any}> {
    const formData = new FormData();

    formData.append('versions', JSON.stringify(versions.map(v => v.name)));
    versions.forEach(v => {
      formData.append(v.name.replace(/ /g, '_'), v.file); // formData keys cannot contain spaces
    });

    formData.append('title', data.title);
    formData.append('description', data.description);
    formData.append('genre', data.genre);
    formData.append('cover', data.cover);

    try {
      await Axios.post(environment.apiUrl + 'audio/upload', formData, {
        onUploadProgress: ((progress: {loaded: number, total: number}) => onProgress(progress.loaded / progress.total * 100)),
        headers: {
          Authorization: localStorage.getItem('token')
        }
      });

      return {success: true};
    } catch (error) {
      console.log(error); // TODO: remove
      return {success: false, error};
    }
  }

  // TODO: finish implementation, I.E. send different options depending on New, Trending etc
  getTracks(options: TrackSearchOptions = {}): Observable<Track[]> {
    return this.http.post<Track[]>(environment.apiUrl + 'music/getTracksWithOptions', options).pipe(
      map(data => data.map(TrackMapper.map))).pipe(catchError(handleError));
  }

  // TODO: this needs to be replaced by an option in the getTracksWithOptions end point - cleaner
  getTracksByFollowedArtists(): Observable<Track[]> {
    return this.http.get<Track[]>(environment.apiUrl + 'music/getTracksByFollowedArtists', {}).pipe(
      map(data => data.map(TrackMapper.map))).pipe(catchError(handleError));
  }

  changeTrack(track: any) {
    this._track = track;
    this.subjectTrack.next(this._track);
  }

  clear() {
    this._track = {};
    this.subjectTrack.next(this._track);
  }

  getAllGenres(): Observable<{name: string}[]> {
    return this.http.get<{name: string}[]>(environment.apiUrl + 'genre/all', {}).pipe(catchError(handleError));
  }
}
