import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

// TODO: convert each error message to user-friendly text
const handleError = (error: HttpErrorResponse) => {
  return throwError(error.message);
};

// TODO: move to dashboard models file?
interface TrackMetric {
  date: string;
  plays: number;
  comments: number;
  likes: number;
  downloads: number;
}

interface GlobalMetric {
  plays: number;
  likes: number;
  shares: number;
  downloads: number;
  followers: number;
}

export enum TimePeriod {
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH'
}

export enum UserStatsTimePeriod {
  TOTAL = 'TOTAL',
  WEEK = 'WEEK'
}

export enum MetricType {
  TRACK = 'TRACK',
  LOCATION = 'LOCATION'
}

export interface TrackSearchOptions {
  new?: boolean;
  trending?: boolean;
  following?: boolean;
  userId?: number;
  genre?: string;
  featured?: boolean;
  page?: number;
  itemsPerPage?: number;
  filter?: {
    title?: string,
    artist?: string
  };
  me?: boolean;
  liked?: boolean;
}

export interface PackageOptions {
  name?: string;
  sub_price?: number;
  track_limit?: number;
  description?: string;
}

export enum SortByOptions {
  LIKES,
  PLAYS,
  DOWNLOADS,
  COMMENTS
}

export {handleError, TrackMetric, GlobalMetric};
