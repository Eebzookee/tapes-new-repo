import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {GlobalMetric, handleError, MetricType, TimePeriod, TrackMetric} from '../helpers';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(
    private http: HttpClient,
  ) { }

  // Time period: TOTAL or WEEK
  getMyGlobalMetrics(timePeriod: string): Observable<GlobalMetric> {
    return this.http.post<GlobalMetric>(environment.apiUrl + 'dashboard/getMyGlobalMetrics', {timePeriod}).pipe(catchError(handleError));
  }

  getMetrics(timePeriod: TimePeriod, type: MetricType, trackId?: number, countryCode?: string): Observable<TrackMetric[]> {
    return this.http.post<TrackMetric[]>(environment.apiUrl + 'dashboard/getMetrics', {type, timePeriod, trackId, countryCode}).pipe(catchError(handleError));
  }

  getMetricsForTodayByTrack(): Observable<TrackMetric[]> {
    return this.http.get<TrackMetric[]>(environment.apiUrl + 'dashboard/getMetricsForTodayByTrack').pipe(catchError(handleError));
  }

  getMetricsForThisMonthByTrack(): Observable<TrackMetric[]> {
    return this.http.get<TrackMetric[]>(environment.apiUrl + 'dashboard/getMetricsForThisMonthByTrack').pipe(catchError(handleError));
  }

  // TODO: implement
  getMetricsForTodayByLocation() {

  }

  // TODO: implement
  getMetricsForThisMonthByLocation() {

  }
}
