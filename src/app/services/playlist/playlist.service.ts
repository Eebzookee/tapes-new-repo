import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Playlist, PlaylistMapper } from 'src/app/models/playlist/playlist.model';
import { environment } from 'src/environments/environment';
import { handleError } from '../helpers';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  constructor(
    private http: HttpClient
  ) { }

  createPlaylist(name: string, tracks: number[] = []): Observable<any> {
    return this.http.post(environment.apiUrl + "playlists/", {name, tracks}).pipe(catchError(handleError));
  }

  getMyPlaylists(): Observable<Playlist[]> {
    return this.http.get<Playlist[]>(environment.apiUrl + "playlists/").pipe(
      map(data => data.map(PlaylistMapper))).pipe(
        catchError(handleError));
  }

  deletePlaylist(): Observable<any> {
    return this.http.post(environment.apiUrl + "playlists/delete", {}).pipe(catchError(handleError));
  }

  addTrackToPlaylist(playlistId: number, trackId: number) : Observable<any> {
    return this.http.post(environment.apiUrl + "playlists/track", {playlistId, trackId}).pipe(catchError(handleError));
  }

  removeTrackFromPlaylist(playlistId: number, trackId: number): Observable<any> {
    return this.http.post(environment.apiUrl + "playlists/removeTrack", {playlistId, trackId}).pipe(catchError(handleError));
  }
}
