import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Package, PackageMapper } from "src/app/models/package/package.model";
import { handleError } from "../helpers";

@Injectable({
  providedIn: "root",
})
export class RevenueService {
  constructor(private http: HttpClient) {}

  getAllPackages(): Observable<any> {
    return this.http
      .get<any>(environment.apiUrl + "revenue/getAllPackages", {})
      .pipe(map((data) => data))
      .pipe(catchError(handleError));
  }
  
  upgradePackage(data:any): Observable<any> {
    return this.http.post(environment.apiUrl + "revenue/updatePackage", data);
  }

  bankAddRecipient(data:any): Observable<any> {
    return this.http.post(environment.apiUrl + "revenue/bankAddRecipient", data);
  }

  // https://sandboxapi.contis.com/Transfer/AddRecipient

}
