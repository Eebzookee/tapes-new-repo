import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {environment} from 'src/environments/environment';
import { NgxPermissionsService } from 'ngx-permissions';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {User, UserMapper} from 'src/app/models/user/user.model';
import {handleError} from '../helpers';
import {Feedback, FeedbackMapper} from '../../models/feedback/feedback.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user = new BehaviorSubject<User | null>(null);

  constructor(
    private http: HttpClient,
    private permissionsService: NgxPermissionsService
  ) {
  }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl + 'user/all').pipe(
      map(data => data.map(UserMapper))).pipe(
        catchError(handleError));
  }

  getMyFeedback(userId: number | null = null): Observable<Feedback[]> {
    return this.http.get<Feedback[]>(environment.apiUrl + 'dj_tokens/getMyFeedback' + (userId ? '/' + userId : '')).pipe(
      map(data => data.map(FeedbackMapper))).pipe(
        catchError(handleError));
  }

  logout() {
    this.user.next(null);
    localStorage.clear();
    this.permissionsService.loadPermissions(['GUEST']);
  }

  attemptUserLogin(email: string, password: string): Observable<string> {
    return this.http.post<string>(environment.apiUrl + 'auth/login', {email, password});
  }

  getUsers(): Observable<User[]> {
    return this.http.post<User[]>(environment.apiUrl + '', {}).pipe(
      map(data => data.map(UserMapper))).pipe(
        catchError(handleError));
  }

  getUser(userId: number): Observable<User> {
    return this.http.get<User>(environment.apiUrl + 'user/getUserById/' + userId)
      .pipe(map(data => UserMapper(data)))
      .pipe(catchError(handleError));
  }

  // TODO: the 3 methods below need to be combined into a single method.
  //  The backend needs a GET endpoint for selecting users based off role, if they are trending etc.
  //  There is no endpoint for getting trending or artists you are following so getTrendingArtists and getFollowedArtists use a placeholder
  //  endpoint
  getFeaturedArtists(): Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl + 'music/getFeaturedArtists', {}).pipe(
      map(data => data.map(UserMapper))).pipe(catchError(handleError));
  }

  getNewArtists(): Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl + 'music/getNewArtists', {}).pipe(
      map(data => data.map(UserMapper))).pipe(
        catchError(handleError));
  }

  getTrendingArtists(): Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl + 'music/getFeaturedArtists', {}).pipe(
      map(data => data.map(UserMapper))).pipe(
        catchError(handleError));
  }

  getFollowedArtists(): Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl + 'music/getFollowedArtists', {}).pipe(
      map(data => data.map(UserMapper))).pipe(
        catchError(handleError));
  }
}
