import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {catchError} from 'rxjs/operators';
import {handleError} from '../helpers';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private http: HttpClient
  ) { }

  giveToken(commentId: number, userId: number): Observable<any> {
    return this.http.post(environment.apiUrl + 'dj_tokens/giveToken', {comment_id: commentId, user_id: userId})
      .pipe(catchError(handleError));
  }

  removeToken(commentId: number): Observable<any> {
    return this.http.post(environment.apiUrl + 'dj_tokens/removeToken', {comment_id: commentId})
      .pipe(catchError(handleError));
  }

  removeFeedback(commentId: number): Observable<any> {
    return this.http.post(environment.apiUrl + 'dj_tokens/removeFeedback', {comment_id: commentId})
      .pipe(catchError(handleError));
  }
}
