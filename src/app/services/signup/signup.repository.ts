import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {environment} from 'src/environments/environment';

@Injectable()
export class SignupRepository {

  constructor(
    private http: HttpClient
  ) {
  }

  register(data: {}): Observable<any> {
    return this.http.post(environment.apiUrl + "auth/registerUser", data);
  }

  updateUser(data:any): Observable<any> {
    return this.http.post(environment.apiUrl + "auth/updateUser", data);
  }

  getCheckoutSessionId(packageId: string, userId?: number): Observable<any> {
    return this.http.post(environment.apiUrl + "stripe/createCheckoutSession", {package_id: packageId, user_id: userId});
  }
}
