import { TestBed } from '@angular/core/testing';

import { TrackPopupService } from './track-popup.service';

describe('TrackPopupService', () => {
  let service: TrackPopupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrackPopupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
