import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Track} from '../../models/track/track.model';

@Injectable({
  providedIn: 'root'
})
export class TrackPopupService {
  track = new BehaviorSubject<Track | null>(null);

  constructor() { }
}
