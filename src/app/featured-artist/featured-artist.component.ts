import {Component, Input, OnInit} from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import { Location } from '@angular/common';
import { HeaderService } from 'src/app/navigation/header/header.service';
import {User} from '../models/user/user.model';

@Component({
  selector: 'app-featured-artist',
  templateUrl: './featured-artist.component.html',
  styleUrls: ['./featured-artist.component.scss']
})
export class FeaturedArtistComponent implements OnInit {

  @Input() users: User[] = [];

  imgags = [
    'assets/bg.jpg',
    'assets/car.png',
    'assets/canberra.jpg',
    'assets/holi.jpg'
  ];
  public carouselTileItems: Array<any> = [0, 1, 2, 3, 4, 5];
  public carouselTiles: any = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: []
  };
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 3, lg: 3, all: 0 },
    slide: 3,
    speed: 250,
    point: {
      visible: true
    },
    load: 2,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)'
  };
  constructor(
    private headerService: HeaderService,
    private _location: Location,
  ) { }

  ngOnInit(): void {
    this.headerService.changePageType('withmenu');
    this.headerService.changePageTitle('News|feed');
    this.carouselTileItems.forEach(el => {
      this.carouselTileLoad(el);
    });
  }

  public carouselTileLoad(j: any) {
    // console.log(this.carouselTiles[j]);
    const len = this.carouselTiles[j].length;
    if (len <= 30) {
      for (let i = len; i < len + 15; i++) {
        this.carouselTiles[j].push(
          this.imgags[Math.floor(Math.random() * this.imgags.length)]
        );
      }
    }
  }

}
