import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import {UserService} from '../services/user/user.service';
import {Track} from '../models/track/track.model';
import {AudioService} from '../services/audio/audio.service';
import {User} from '../models/user/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tracksFeatured: Track[] = [];
  artistsFeatured: User[] = [];

  tracksNew: Track[] = [];
  artistsNew: User[] = [];

  artistsTrending: User[] = [];
  tracksTrending: Track[] = [];

  tracksFollowedArtists: Track[] = [];

  constructor(
    private audioService: AudioService,
    private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getNewArtists().subscribe((response) => {
      this.artistsNew = response;
    });

    this.audioService.getTracksByFollowedArtists().subscribe((response) => {
      this.tracksFollowedArtists = response;
    }, (error) => {
      // TODO: display message to user
      console.log(error);
    });

    this.userService.getFeaturedArtists().subscribe((response) => {
      this.artistsFeatured = response;
      this.artistsTrending = response; // TODO: endpoint for getting trending artists needs to be made. This is placeholder
    }, (error) => {
      // TODO: display message to user
      console.log(error);
    });

    // Fall back for end points that haven't been made yet
    this.audioService.getTracks().subscribe((response) => {
      this.tracksFeatured = response;
      this.tracksTrending = response;
      this.tracksNew = response;
    }, (error) => {
      // TODO: display message to user
      console.log(error);
    });
  }
}
