import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SignupUserDetailsStepComponent} from './signup-user-details-step.component';

describe('SignupUserDetailsStepComponent', () => {
  let component: SignupUserDetailsStepComponent;
  let fixture: ComponentFixture<SignupUserDetailsStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignupUserDetailsStepComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupUserDetailsStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
