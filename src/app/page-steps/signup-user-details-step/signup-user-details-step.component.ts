import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-signup-user-details-step',
  templateUrl: './signup-user-details-step.component.html',
  styleUrls: ['./signup-user-details-step.component.scss']
})
export class SignupUserDetailsStepComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  @Input() barvalue!: number;

  constructor() {
  }

  ngOnInit() {
    this.barvalue = 50;
    //this.audioRepository.setCount.subscribe((msg: any) => {
    //  this.barvalue = this.barvalue + parseInt(msg, 10);
    //})
  }

  public checkError = (controlName: string, errorName: string) => {
    //console.log('controlName', controlName, errorName);
    //console.log('this.formGroup.controls[controlName].hasError(errorName)', this.formGroup.controls[controlName].hasError(errorName));
    return this.formGroup.controls[controlName].hasError(errorName);
  }

}
