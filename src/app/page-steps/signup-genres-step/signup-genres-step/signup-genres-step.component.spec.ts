import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupGenresStepComponent } from './signup-genres-step.component';

describe('SignupGenresStepComponent', () => {
  let component: SignupGenresStepComponent;
  let fixture: ComponentFixture<SignupGenresStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupGenresStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupGenresStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
