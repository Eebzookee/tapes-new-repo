import { Component, Input, OnInit } from '@angular/core';
//import {GenreRepository} from "src/app/genre/genre.repository";
import {Genre} from "src/app/genre/genre.model";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-signup-genres-step',
  templateUrl: './signup-genres-step.component.html',
  styleUrls: ['./signup-genres-step.component.scss']
})
export class SignupGenresStepComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  genres!: Genre[];

  constructor(
    //private genreRepository: GenreRepository,
    //private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.genres = [
      {id:3, name:"Hip Hop", checked:false,  touched:false},
      {id:4, name:"Grime", checked:false,  touched:false},
      {id:6, name:"Rap", checked:false,  touched:false},
      {id:7, name:"Drill", checked:false,  touched:false},
      {id:8, name:"Afro", checked:false,  touched:false},
      {id:11,name:"Trap", checked:false,  touched:false},
      {id:13,name:"R&B", checked:false,  touched:false}
    ];

  }

  public checkError = (controlName: string, errorName: string) => {
    //console.log('controlName', controlName, errorName);
    //console.log('this.formGroup.controls[controlName].hasError(errorName)', this.formGroup.controls[controlName].hasError(errorName));
    return this.formGroup.controls[controlName].hasError(errorName);
  }

}
