import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { ProfilePageComponent } from '../user/profile-page/profile-page.component';
import { LoginComponent } from '../user/login/login.component';
import { RegisterComponent } from '../user/register/register.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { DashboardComponent } from '../user/dashboard/dashboard.component';
import { WelcomeComponent } from '../user/welcome/welcome.component';
import { LibraryComponent } from '../user/library/library.component';
import { DjProfilePageComponent } from '../user/dj-profile-page/dj-profile-page.component';
import { ViewUsersComponent } from '../admin/view-users/view-users.component';
import { SearchComponent } from '../search/search.component';
import { UpgradeComponent } from '../user/upgrade/upgrade.component';


const routes: Routes = [
  /*
  { path: 'home', component: HomeComponent},
  */

  {
    path: 'home',
    component: HomeComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: 'GUEST',
        redirectTo: '/user/profile'
      }
    }
  }, 
  {
    path: '',
    component: HomeComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: 'GUEST',
        redirectTo: '/user/profile'
      }
    }
  },  
  {
    path: 'user/profile',
    component: ProfilePageComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['GUEST', 'USER', 'ADMIN'],
        redirectTo: '/user/login'
      }
    }
  },
  {
    path: 'user/profile/:userId',
    component: ProfilePageComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['GUEST', 'USER', 'ADMIN'],
        redirectTo: '/user/login'
      }
    }
  },
  {
    path: 'user/dj-profile/:userId',
    component: DjProfilePageComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: 'GUEST',
        redirectTo: '/user/login'
      }
    }
  },
  {
    path: 'user/dj-profile',
    component: DjProfilePageComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: 'GUEST',
        redirectTo: '/user/login'
      }
    }
  },
  {
    path: 'user/upgrade',
    component: UpgradeComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['GUEST', 'USER', 'ADMIN'],
        redirectTo: '/user/upgrade'
      }
    }
  },
  {
    path: 'user/login',
    component: LoginComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'GUEST',
        redirectTo: '/user/profile'
      }
    }
  },
  {
    path: 'user/register',
    component: RegisterComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'GUEST',
        redirectTo: '/home'
      }
    }
  },
  {
    path: 'user/dashboard',
    component: DashboardComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: 'GUEST',
        redirectTo: '/user/login'
      }
    }
  },
  {
    path: 'user/welcome',
    component: WelcomeComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: 'GUEST',
        redirectTo: '/user/login'
      }
    }
  },
  {
    path: 'user/library',
    component: LibraryComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        except: 'GUEST',
        redirectTo: '/user/login'
      }
    }
  },
  {
    path: 'admin/users',
    component: ViewUsersComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: 'ADMIN',
        redirectTo: '/home'
      }
    }
  },
  {
    path: 'search',
    component: SearchComponent
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
