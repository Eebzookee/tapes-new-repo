import {Component, Input, OnInit} from '@angular/core';
import {TrackPopupService} from '../../services/track-popup/track-popup.service';
import {Track} from '../../models/track/track.model';
import {Router} from '@angular/router';
import {AudioService} from '../../services/audio/audio.service';
import {Playlist} from '../../models/playlist/playlist.model';

@Component({
  selector: 'app-track-popup',
  templateUrl: './track-popup.component.html',
  styleUrls: ['./track-popup.component.scss']
})
export class TrackPopupComponent implements OnInit {

  constructor(
    private trackPopupService: TrackPopupService,
    private router: Router,
    private audioService: AudioService
  ) { }

  track: Track | null = null;
  @Input() showDeletePlaylistButton = false;
  showShare = false;
  showComments = false;
  showAddToPlaylist = false;
  showDownload = false;
  playlists: Playlist[] = [];
  @Input() deleteSongFromPlaylistCallback = () => {};

  ngOnInit(): void {
    this.trackPopupService.track.subscribe(t => {
      this.track = t;
    });
  }

  close() {
    if (this.showShare) {
      this.showShare = false;
    } else if (this.showAddToPlaylist) {
      this.showAddToPlaylist = false;
    } else if (this.showComments) {
      this.showComments = false;
    } else if (this.showDownload) {
      this.showDownload = false;
    } else {
      this.trackPopupService.track.next(null);
    }
  }

  like() {
    if (this.track) {
      this.audioService.toggleLike(this.track.id).subscribe(() => {
        if (this.track) {
          this.track.liked = !this.track.liked;
        }
      }, error => console.log(error)); // TODO: move to toast message
    }
  }

  addToPlaylist() {
    this.showAddToPlaylist = true;
  }

  share() {
    this.showShare = true;
  }

  // TODO: implement
  shareOnClick(type: string) {

  }

  viewArtist() {
    this.router.navigateByUrl(`/user/profile/${this.track?.user.id}`).then(() => {
      this.trackPopupService.track.next(null);
    });
  }

  comments() {
    this.showComments = true;
  }

  // TODO: implement
  download() {
    this.showDownload = true;
  }
}
