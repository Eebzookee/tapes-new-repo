import {Component, Input, OnInit} from '@angular/core';
import {CreatePlaylistDialogComponent} from './create-playlist-dialog/create-playlist-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {PlaylistService} from '../../../services/playlist/playlist.service';
import {Playlist} from '../../../models/playlist/playlist.model';
import {Track} from '../../../models/track/track.model';

@Component({
  selector: 'app-add-to-playlist',
  templateUrl: './add-to-playlist.component.html',
  styleUrls: ['./add-to-playlist.component.scss']
})
export class AddToPlaylistComponent implements OnInit {

  loading = true;
  playlists: Playlist[] = [];
  @Input() track: Track | null = null;

  constructor(
    private dialog: MatDialog,
    private playlistService: PlaylistService
  ) { }

  ngOnInit(): void {
    this.fetchPlaylists();
  }

  fetchPlaylists() {
    this.playlistService.getMyPlaylists().subscribe(response => {
      this.playlists = response;
      this.loading = false;
    }, error => {
      console.log(error);
      this.loading = false;
    });
  }

  createPlaylist() {
    this.dialog.open(CreatePlaylistDialogComponent).afterClosed().subscribe(() => {
      setTimeout(() => {
        this.loading = true;
        this.fetchPlaylists();
      }, 500);
    });
  }

  addTrackToPlaylist(playlist: Playlist) {
    if (this.track) {
      this.playlistService.addTrackToPlaylist(playlist.id, this.track.id).subscribe(() => {
        // TODO: show toast
        this.loading = true;
        this.fetchPlaylists();
      }, error => console.log(error));
    }
  }

}
