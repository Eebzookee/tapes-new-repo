import { Component, OnInit } from '@angular/core';
import {PlaylistService} from '../../../../services/playlist/playlist.service';

@Component({
  selector: 'app-create-playlist-dialog',
  templateUrl: './create-playlist-dialog.component.html',
  styleUrls: ['./create-playlist-dialog.component.scss']
})
export class CreatePlaylistDialogComponent implements OnInit {

  playlistName = '';

  constructor(
    private playlistService: PlaylistService
  ) { }

  ngOnInit(): void {
  }

  createPlaylist() {
    this.playlistService.createPlaylist(this.playlistName).subscribe(response => {
      console.log(response);
    }, error => console.log(error));
  }
}
