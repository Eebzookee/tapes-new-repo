import {Component, Input, OnInit} from '@angular/core';
import {Track} from '../../../models/track/track.model';
import {AudioService} from '../../../services/audio/audio.service';
import {MatDialog} from '@angular/material/dialog';
import {AddCommentDialogComponent} from './add-comment-dialog/add-comment-dialog.component';
import {TrackComment} from '../../../models/feedback/feedback.model';
import {timeInterval, timeout} from 'rxjs/operators';
import {interval} from 'rxjs';

@Component({
  selector: 'app-track-comments',
  templateUrl: './track-comments.component.html',
  styleUrls: ['./track-comments.component.scss']
})
export class TrackCommentsComponent implements OnInit {
  loading = true;
  @Input() track: Track | null = null;
  comments: TrackComment[] = [];

  constructor(
    private audioService: AudioService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.fetchComments();
  }

  fetchComments() {
    if (this.track) {
      this.audioService.getTrackComments(this.track.id).subscribe(response => {
        this.comments = response.sort((a, b) => {
          return b.date.getTime() - a.date.getTime();
        });
        this.loading = false;
      }, error => {
        console.log(error);
        this.loading = false;
      });
    }
  }

  createComment() {
    this.dialog.open(AddCommentDialogComponent, {data: {track: this.track}}).afterClosed().subscribe(() => {
      setTimeout(() => {
        this.comments = [];
        this.loading = true;
        this.fetchComments();
      }, 300);
    });
  }

}
