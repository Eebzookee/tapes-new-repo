import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackCommentsComponent } from './track-comments.component';

describe('TrackCommentsComponent', () => {
  let component: TrackCommentsComponent;
  let fixture: ComponentFixture<TrackCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
