import {Component, Inject, Input, OnInit} from '@angular/core';
import {AudioService} from '../../../../services/audio/audio.service';
import {Track} from '../../../../models/track/track.model';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-comment-dialog',
  templateUrl: './add-comment-dialog.component.html',
  styleUrls: ['./add-comment-dialog.component.scss']
})
export class AddCommentDialogComponent implements OnInit {

  comment = '';

  // TODO: get comment data!

  constructor(
    private audioService: AudioService,
    @Inject(MAT_DIALOG_DATA) private data: {track: Track}
  ) { }

  ngOnInit(): void {
  }

  addComment() {
    this.audioService.addComment(this.data.track.id, this.comment).subscribe((response) => {
      console.log(response);
    }, error => console.log(error));
  }
}
