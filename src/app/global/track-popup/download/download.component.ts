import {Component, Input, OnInit} from '@angular/core';
import {Track} from '../../../models/track/track.model';
import {AudioService} from '../../../services/audio/audio.service';
import {saveAs} from 'file-saver';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

  @Input() track: Track | null = null;
  selectedVersion = this.track?.versions[0].id;
  vtrack = this.track!

  constructor(
    private audioService: AudioService
  ) { }

  ngOnInit(): void {
  }

  download() {
    if (!this.track || !this.track.id) {
      return;
    }

    const fileName = this.track.user.name + ' - ' + this.track.title; // TODO make sure this works for wavs and mp3s

    this.audioService.download(this.track.selectedVersion).subscribe(
      data => {
        saveAs(data, fileName);
      }
    );
  }
}
