import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ButtonModule, DialogModule, InputTextModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';

import {DirectiveModule} from '~app/shared/directives/directive.module';
import {GenreRoutingModule} from './genre-routing.module';
import {GenreComponent} from './genre.component';
import {GenreRepository} from './genre.repository';

@NgModule({
  imports: [
    CommonModule,
    GenreRoutingModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    TableModule,
    DirectiveModule,
    DialogModule,
    ReactiveFormsModule,
    TranslateModule.forChild()
  ],
  declarations: [
    GenreComponent
  ],
  exports: [
    GenreComponent
  ],
  providers: [
    GenreRepository
  ]
})
export class GenreModule {
}
