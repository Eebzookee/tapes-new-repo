import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

//import {PagingList, PagingMapper} from 'src/appapp/shared';
import {Genre, GenreId} from './genre.model';
import {GenreMapper} from './genre.mapper';

@Injectable()
export class GenreRepository {

  constructor(
    private http: HttpClient
  ) {
  }

  public getById(genreId: GenreId): Observable<Genre> {
    return this.http.get<Genre>(
      'genre/id/' + genreId
    ).pipe(
      map(GenreMapper.map)
    );
  }

  public getAll(): Observable<Genre[]> {
    return this.http.get<Genre[]>(
      'genre/all'
    ).pipe(
      map(items => items.map(GenreMapper.map))
    );
  }
  /*
  public getList(page: number = 0, listSize: number = 5): Observable<PagingList<Genre>> {
    return this.http.get<PagingList<Genre>>(
      'genre/list/' + listSize + '/' + page,
    )
      .pipe(
        map(
          data => {
            return PagingMapper.map<Genre>(
              data,
              data.content.map(GenreMapper.map)
            )
          }
        )
      );
  }

  public create(name: string): Observable<any> {
    return this.http.post(
      'genre/create',
      {
        name
      }
    );
  }
  */
  public update(genreId: GenreId, data: any): Observable<any> {
    return this.http.put(
      'genre/update/' + genreId,
      data
    );
  }

  public delete(genreId: GenreId): Observable<any> {
    return this.http.delete(
      'genre/' + genreId
    );
  }

}
