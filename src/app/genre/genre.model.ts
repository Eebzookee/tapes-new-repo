export class Genre {

  public checked: boolean = false;
  public touched: boolean = false;

  constructor(
    public id: GenreId,
    public name: string
  ) {
  }
}

export type GenreId = number;
