import {Genre} from "./genre.model";

export class GenreMapper {

  public static map(data: any): Genre {
    if (!data) {
      return {id:0, name:"", checked:false,  touched:false};
    }

    return new Genre(
      data.id,
      data.name,
    )
  }
}


