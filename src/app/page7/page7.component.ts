import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Location } from "@angular/common";
import { HeaderService } from "../navigation/header/header.service";

@Component({
  selector: 'app-page7',
  templateUrl: './page7.component.html',
  styleUrls: ['./page7.component.scss']
})
export class Page7Component implements OnInit {

  public dj:boolean = false;
  public artist:boolean = false;
  public starter:boolean = true;

  constructor(
    private headerService: HeaderService,
    private _location: Location
  ) {}

  ngOnInit(): void {
    this.headerService.changePageType("withmenu");
    this.headerService.changePageTitle("page 7");    
  }

  backClicked() {
    this._location.back();
  }

  chooseUserType(usertype:string){
    this.starter = false;
    this.dj = false;
    this.artist = false;
    usertype === 'dj' ? (this.dj = true, this.artist = false) : (this.artist = true, this.dj = false);
  }

}
