import { AbstractControl } from "@angular/forms";

export function urlValidator(control: AbstractControl) {
  if (!control.value.startsWith("https") || !control.value.includes(".me")) {
    return { urlValid: true };
  }
  return null;
}

export function speshCharValidator(control: AbstractControl) {
  let format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  if (!format.test(control.value)) {
    return { speshValid: true };
  }
  return null;
}

/*
export function passwordValidator(control: AbstractControl, control2: AbstractControl) {
  
  if (control.value !== control2.value)) {
    return { passwordValid: true };
  }
  return null;
}
*/
