import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonModule} from 'primeng/primeng';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';

import {PageNotFoundComponent} from './page-not-found.component';

@NgModule({
  declarations: [PageNotFoundComponent],
  imports: [
    CommonModule,
    ButtonModule,
    RouterModule,
    TranslateModule.forChild()
  ]
})
export class PageNotFoundModule {
}
