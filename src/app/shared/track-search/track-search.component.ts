import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {AudioService} from '../../services/audio/audio.service';
import {Track} from '../../models/track/track.model';
import {SortByOptions} from '../../services/helpers';

@Component({
  selector: 'app-track-search',
  templateUrl: './track-search.component.html',
  styleUrls: ['./track-search.component.scss']
})
export class TrackSearchComponent implements AfterViewInit {

  @Input() userId: number | null = null;
  @Input() hideClose = false;

  sortByOptions: {label: string, value: SortByOptions}[] = [
    {
      label: 'Likes',
      value: SortByOptions.LIKES
    },
    {
      label: 'Downloads',
      value: SortByOptions.DOWNLOADS
    },
    {
      label: 'Plays',
      value: SortByOptions.PLAYS
    },
    {
      label: 'Comments',
      value: SortByOptions.COMMENTS
    }
  ];
  selectedSortByOption = SortByOptions.LIKES;

  searchField = '';
  tracks: Track[] = [];

  constructor(
    private audioService: AudioService
  ) { }

  ngAfterViewInit(): void {
    this.fetchData();
    console.log(this.userId)
  }

  fetchData() {
    if (!this.userId) return;
    // TODO: could optimise by only sending request to backend if the search field has changed not the sort by option
    this.audioService.getTracks({
      userId: this.userId,
      filter: this.searchField ? {
        title: this.searchField
      } : undefined
    }).subscribe(tracks => {
      this.tracks = tracks.sort((a, b) => {
        switch (this.selectedSortByOption) {
          case SortByOptions.PLAYS:
            return b.metrics.numberPlays.value - a.metrics.numberPlays.value;
          case SortByOptions.COMMENTS:
            return b.metrics.numberComment.value - a.metrics.numberComment.value;
          case SortByOptions.DOWNLOADS:
            return b.metrics.numberDownloads.value - a.metrics.numberDownloads.value;
          case SortByOptions.LIKES:
            return b.metrics.numberLikes.value - a.metrics.numberLikes.value;
        }
      });
    }, error => {
      console.log(error);
    });
  }
}
