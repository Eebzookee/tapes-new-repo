import {SelectItem} from "primeng/primeng";

export class SelectItemMapper {
  public static map(label: string, value?: any): SelectItem {
    if (!label) {
      return;
    }

    return {
      label: label,
      value: value ? value : label
    }

  }

}


