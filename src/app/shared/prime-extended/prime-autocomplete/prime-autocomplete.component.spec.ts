import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PrimeAutocompleteComponent} from './prime-autocomplete.component';

describe('PrimeAutocompleteComponent', () => {
  let component: PrimeAutocompleteComponent;
  let fixture: ComponentFixture<PrimeAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrimeAutocompleteComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
