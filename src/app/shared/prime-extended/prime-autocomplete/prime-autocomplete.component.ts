import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {AutoComplete, SelectItem} from 'primeng/primeng';
import {filter} from 'lodash-es';

import {DELAY_TIME} from '../config';

@Component({
  selector: 'prime-autocomplete',
  templateUrl: './prime-autocomplete.component.html',
  styleUrls: ['./prime-autocomplete.component.scss']
})
export class PrimeAutocompleteComponent {

  @ViewChild('autocomplete')
  public autocomplete: AutoComplete;

  @Output()
  public onControlChange: EventEmitter<string[]> = new EventEmitter();

  @Input()
  public label: string;

  @Input()
  public value: SelectItem[];

  @Input()
  public errors: any;

  @Input()
  public multiple: boolean = false;

  @Input()
  public expandable: boolean = false;

  @Input()
  public suggestions: Observable<SelectItem[]>;

  public control: any;

  public form: FormGroup;
  public DELAY_TIME = DELAY_TIME;
  public filteredSuggestions = [];

  private suggestionBase: any[];
  private formChange: Subscription = null;
  private isLoading: boolean = false;

  constructor() {
  }

  ngOnDestroy() {
    if (this.formChange) {
      this.formChange.unsubscribe();
    }
  }

  public addOption(event: KeyboardEvent): void {
    if (event.keyCode === 13 && this.expandable) {
      const newValue: string = this.autocomplete.multiInputEL.nativeElement.value;

      if (!newValue) {
        return;
      }

      let value: string[] = this.control || [];

      this.autocomplete.multiInputEL.nativeElement.value = '';

      if (value.indexOf(newValue) !== -1) {
        return;
      }

      this.control = value.concat(newValue);

      this.emit();
    }
  }

  ngOnChanges() {
    if (this.value && !this.control) {
      this.control = this.value;
      this.emit();
    }

    this.assignSuggestions();
  }

  public filterItems(query: string): void {
    this.filteredSuggestions = [];

    this.filteredSuggestions = filter(this.suggestionBase,
      item => {
        return item.label.toLowerCase().indexOf(query.toLowerCase()) === 0
      }
    );
  }

  public emit(): void {
    this.onControlChange.emit(this.control.map(selectItem => selectItem.value));
  }

  private assignSuggestions(): void {
    if (!this.suggestions || this.suggestionBase || this.isLoading === true) {
      return;
    }

    this.isLoading = true;

    this.suggestions.subscribe(
      suggestions => {
        this.suggestionBase = suggestions || []
        this.isLoading = false;
      }
    );
  }
}
