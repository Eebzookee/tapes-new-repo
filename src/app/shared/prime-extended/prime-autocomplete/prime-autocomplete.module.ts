import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {AutoCompleteModule} from 'primeng/primeng';

import {ErrorDisplayModule} from '~app/shared/error-display/error-display.module';
import {PrimeAutocompleteComponent} from './prime-autocomplete.component';

@NgModule({
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule,
    ErrorDisplayModule
  ],
  declarations: [PrimeAutocompleteComponent],
  exports: [PrimeAutocompleteComponent]
})
export class PrimeAutocompleteModule {
}
