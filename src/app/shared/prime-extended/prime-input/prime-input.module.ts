import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputMaskModule, InputTextModule} from 'primeng/primeng';
import {ReactiveFormsModule} from '@angular/forms';

import {ErrorDisplayModule} from '~app/shared/error-display/error-display.module';
import {PrimeInputComponent} from './prime-input.component';

@NgModule({
  imports: [
    CommonModule,
    InputMaskModule,
    InputTextModule,
    ReactiveFormsModule,
    ErrorDisplayModule
  ],
  declarations: [PrimeInputComponent],
  exports: [PrimeInputComponent]
})
export class PrimeInputModule {
}
