import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'prime-input',
  templateUrl: './prime-input.component.html',
  styleUrls: ['./prime-input.component.scss']
})
export class PrimeInputComponent implements OnInit, OnDestroy {

  @Output()
  public onControlChange: EventEmitter<AbstractControl> = new EventEmitter();

  @Input()
  public label: string;

  @Input()
  public type: string;

  @Input()
  public mask: string = '';

  @Input()
  public icon: string = '';

  @Input()
  public formControl: AbstractControl;

  public form: FormGroup;
  public maxLength: number;
  public max: number;
  public min: number;

  private formChange: Subscription = null;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy() {
    if (this.formChange) {
      this.formChange.unsubscribe();
    }
  }

  public getMaxLength(): number {
    if (this.maxLength) {
      return this.maxLength;
    }

    try {
      this.maxLength = this.formControl.errors.maxlength.requiredLength + 1;

      return this.maxLength;
    } catch (e) {
      return 100;
    }
  }

  private onChanges(): void {
    this.formChange = this.form
      .valueChanges
      .pipe(debounceTime(500))
      .subscribe(
        () => {
          this.onControlChange.emit(this.form.get('control').value)
        }
      );
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      control: this.formControl
    });

    this.onChanges();
  }
}
