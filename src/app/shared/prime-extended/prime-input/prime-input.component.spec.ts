import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PrimeInputComponent} from '~app/shared/prime-extended/prime-input/prime-input.component';

describe('PrimeInputComponent', () => {
  let component: PrimeInputComponent;
  let fixture: ComponentFixture<PrimeInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrimeInputComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
