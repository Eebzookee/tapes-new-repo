import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {DELAY_TIME} from '../config';

@Component({
  selector: 'prime-textarea',
  templateUrl: './prime-textarea.component.html',
  styleUrls: ['./prime-textarea.component.scss']
})
export class PrimeTextareaComponent implements OnInit, OnDestroy {

  @Output()
  public onControlChange: EventEmitter<AbstractControl> = new EventEmitter();

  @Input()
  public label: string;

  @Input()
  public formControl: AbstractControl;

  public form: FormGroup;

  private formChange: Subscription = null;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnDestroy() {
    if (this.formChange) {
      this.formChange.unsubscribe();
    }
  }

  private onChanges(): void {
    this.formChange = this.form
      .valueChanges
      .pipe(debounceTime(DELAY_TIME))
      .subscribe(
        () => this.onControlChange.emit(this.form.get('control').value)
      );
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      control: this.formControl
    });

    this.onChanges();
  }
}
