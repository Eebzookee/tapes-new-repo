import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputTextareaModule} from 'primeng/primeng';
import {ReactiveFormsModule} from '@angular/forms';

import {PrimeTextareaComponent} from './prime-textarea.component';
import {ErrorDisplayModule} from '~app/shared/error-display/error-display.module';

@NgModule({
  imports: [
    CommonModule,
    InputTextareaModule,
    ReactiveFormsModule,
    ErrorDisplayModule
  ],
  declarations: [PrimeTextareaComponent],
  exports: [PrimeTextareaComponent]
})
export class PrimeTextareaModule {
}
