export enum FILTER_BAR_TYPE {
  CALENDAR = 'calendar',
  INPUT = 'input',
  DROPDOWN = 'dropdown'
}
