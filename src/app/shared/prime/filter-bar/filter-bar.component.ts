import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {CALENDAR} from '../config';
import {PrimeColumn} from '../prime-column.model';
import {FILTER_BAR_TYPE} from './filter-bar-type.enum';

@Component({
  selector: 'app-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.scss']
})
export class FilterBarComponent {

  @Input() columns: PrimeColumn[] = [];
  CALENDAR = CALENDAR;
  FILTER_BAR_TYPE = FILTER_BAR_TYPE;
  filters: any = {};
  @Output()
  private onFilter: EventEmitter<any> = new EventEmitter();
  private timer: any;

  constructor(
    private translate: TranslateService
  ) {
  }

  getLabel(header: string) {
    return this.translate.instant('general.search') + header.toLowerCase();
  }

  filter(field: string, query): void {
    this.filters[field] = query;


    this.emit();
  }

  clear(): void {
    this.filters = {};

    this.emit();
  }

  private emit(): void {
    if (this.timer) {
      clearTimeout(this.timer);

      this.timer = null;
    }

    this.timer = setTimeout(
      () => this.onFilter.emit(this.filters), 500
    );

  }

}
