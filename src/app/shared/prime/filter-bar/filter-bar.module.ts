import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FilterBarComponent} from './filter-bar.component';
import {TranslateModule} from '@ngx-translate/core';
import {CalendarModule, DropdownModule, InputTextModule} from 'primeng/primeng';
import {DirectiveModule} from '../../directives/directive.module';
import {PipeModule} from '../../pipe/pipe.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    DirectiveModule,
    FormsModule,
    DropdownModule,
    PipeModule,
    CalendarModule,
    TranslateModule.forChild()
  ],
  declarations: [
    FilterBarComponent
  ],
  exports: [
    FilterBarComponent
  ]
})
export class FilterBarModule {
}
