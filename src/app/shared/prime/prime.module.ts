import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableModule} from 'primeng/table';
import {TranslateModule} from '@ngx-translate/core';

import {PrimeTableComponent} from './prime-table/prime-table.component';
import {FilterBarModule} from './filter-bar/filter-bar.module';
import {PipeModule} from '../pipe/pipe.module';

@NgModule({
  declarations: [PrimeTableComponent],
  imports: [
    CommonModule,
    TableModule,
    PipeModule,
    FilterBarModule,
    TranslateModule.forChild()
  ],
  exports: [
    PrimeTableComponent
  ]
})
export class PrimeModule {
}
