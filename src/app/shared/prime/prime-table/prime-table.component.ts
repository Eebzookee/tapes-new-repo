import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {PagingList} from '~app/shared/paging/paging-list.model';
import {PrimeTable} from '../prime-table.mode';
import {PrimeColumnModifier} from '../prime-column.model';

@Component({
  selector: 'prime-table',
  templateUrl: './prime-table.component.html',
  styleUrls: ['./prime-table.component.scss']
})
export class PrimeTableComponent implements OnInit {

  @Input() table: PrimeTable;
  @Input() list: PagingList<any>;

  filters: any;
  page: number = 0;

  constructor(
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    if (!this.list) {
      this.loadList();
    }
  }

  changePage(event: any): void {
    this.page = event.first / event.rows;

    this.loadList();
  }

  filter(filters: any): void {
    this.filters = filters;
    this.loadList();
  }

  translateLabel(translateObject: string, translateKey: string): string {
    try {
      return this.translate.instant(translateObject + '.' + translateKey.toLowerCase());
    } catch (error) {
      return '';
    }
  }

  runModifierCommand(modifier: PrimeColumnModifier, itemId: any): void {
    if (!modifier.command) {
      return;
    }

    modifier.command(itemId);
  }

  private loadList(): void {
    if (!this.table || !this.table.loadFn) {
      return;
    }

    this.table.loadFn(this.page, this.table.listSize, {...this.table.filters, ...this.filters}).subscribe(
      list => this.list = list
    );
  }
}
