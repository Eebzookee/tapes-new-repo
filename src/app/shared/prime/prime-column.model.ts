import {SelectItem} from './select-item.model';

export class PrimeColumn {
  constructor(
    public field?: string,
    public filter?: PrimeColumnFilter,
    public header?: string,
    public modifier?: PrimeColumnModifier,
    public options?: any[],
    public style?: string,
  ) {
  }
}

export class PrimeColumnModifier {
  constructor(
    public name: string,
    public value?: any,
    public command?: any
  ) {
  }
}

export class PrimeColumnFilter {
  constructor(
    public type: string,
    public options?: SelectItem<any>[]
  ) {
  }
}
