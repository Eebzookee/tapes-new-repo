export enum CONFIG {
  DATE_FORMAT = 'DD/MM/YYYY',
  ANGULAR_DATE_FORMAT = 'dd/MM/yyyy',
  ROWS_PER_PAGE = 15
}

export const CALENDAR = {
  firstDayOfWeek: 1,
  dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
  dayNamesShort: ["Nie", "Pon", "Wto", "Śro", "Czw", "Pią", "Sob"],
  dayNamesMin: ["Ni", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
  monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
  monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
  today: 'Dziś',
  clear: 'Wyczyść',
  dateFormat: 'dd/mm/yy'
}

