import {filter} from 'lodash-es';

export class SelectItem<T> {
  constructor(
    public label: string,
    public value: T
  ) {
  }
}

export class SelectItemMapper {
  public static map<T>(label: string, value: T): SelectItem<T> {
    if (!label) {
      return;
    }

    return new SelectItem<T>(
      label,
      value
    )

  }

  public static filter<T>(items: SelectItem<T>[], query: string): SelectItem<T>[] {
    if (!query) {
      return;
    }

    return filter(items, item => {
      if (item && item.label) {
        return item.label.toLowerCase().indexOf(query.toLowerCase()) !== -1;
      }
    });
  }

}
