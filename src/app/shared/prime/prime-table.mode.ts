import {PrimeColumn} from './prime-column.model';
import {PagingList} from '../paging/paging-list.model';

export class PrimeTable {
  constructor(
    public columns?: PrimeColumn[],
    public list?: PagingList<any>,
    public listSize?: number,
    public filterBarVisibility?: boolean,
    public filters?: any,
    public loadFn?: any
  ) {
  }
}
