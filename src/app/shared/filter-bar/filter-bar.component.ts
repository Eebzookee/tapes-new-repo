import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {forEach, mapValues} from 'lodash-es';
import {FILTER_BAR_TYPE} from './filter-bar-type.enum';
import {UserService} from '~app/user/user.service';

@Component({
  selector: 'app-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.scss']
})
export class FilterBarComponent {

  @Input()
  public filters: any[] = [];

  @Output()
  public onFilter: EventEmitter<any> = new EventEmitter();

  public form: FormGroup;

  public FILTER_BAR_TYPE = FILTER_BAR_TYPE;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.createForm(this.filters);
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  onChange() {
    this.userService.emitChange({changed: true});
    this.search();
  }

  search(): void {
    const values = mapValues(this.form.value, value => value ? value : null);
    this.onFilter.emit(values);
  }

  reset(): void {
    this.form.reset();
    this.search();
  }

  private createForm(filters: string[]): void {
    this.form = this.formBuilder.group({});

    forEach(filters, (filter: any) => {
      this.form.addControl(filter.name, new FormControl(''));

    });

    this.search();
  }
}
