import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FilterBarComponent} from './filter-bar.component';
import {TranslateModule} from '@ngx-translate/core';
import {ButtonModule, DropdownModule, InputTextModule} from 'primeng/primeng';
import {ReactiveFormsModule} from '@angular/forms';
import {DirectiveModule} from '../directives/directive.module';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    ReactiveFormsModule,
    DirectiveModule,
    DropdownModule,
    ButtonModule,
    TranslateModule.forChild()
  ],
  declarations: [
    FilterBarComponent
  ],
  exports: [
    FilterBarComponent
  ]
})
export class FilterBarModule {
}
