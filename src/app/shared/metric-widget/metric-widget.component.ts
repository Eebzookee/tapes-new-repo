import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-metric-widget',
  templateUrl: './metric-widget.component.html',
  styleUrls: ['./metric-widget.component.scss']
})
export class MetricWidgetComponent implements OnInit {

  @Input() value: number = 0;
  @Input() label: string = "";

  rand = Math.random() * 100;

  constructor() { }

  ngOnInit(): void {
  }

}
