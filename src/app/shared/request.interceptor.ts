import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import {tap} from 'rxjs/operators';

import {environment} from '../../environments/environment';
import {NotifyService} from './notify/notify.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  private jwtHelper = new JwtHelperService();

  constructor(
    private router: Router,
    private notify: NotifyService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


    let interceptedRequest: HttpRequest<any> = request;
    if (request.url !== 'https://restcountries.eu/rest/v2/all') {
      interceptedRequest = this.updateAuthorization(request);
    }

    interceptedRequest = this.updateUrl(interceptedRequest);

    return next.handle(interceptedRequest).pipe(
      tap(
        () => {
        },
        error => this.clearUnauthorized(error)
      )
    );
  }

  private updateAuthorization(request: HttpRequest<any>): HttpRequest<any> {
    this.clearExpiredToken();

    if (this.getToken()) {
      request = request.clone({
        setHeaders: {
          'Authorization': this.getToken(),
          'Content-Type': 'application/json'
        }
      });
    }

    return request;
  }

  private updateUrl(request: HttpRequest<any>): HttpRequest<any> {
    if (this.isTranslationRequest(request.url)) {
      return request;
    }

    if (request.url.indexOf('https://') !== -1) {
      return request;
    }

    if (request.url.indexOf('http://') !== -1) {
      return request;
    }

    return request.clone({
      url: environment.apiUrl + request.url
    });
  }

  private isTranslationRequest(url: string): boolean {
    if (url.indexOf('/assets/i18n') === -1) {
      return false;
    }

    return true;
  }

  private clearExpiredToken(): void {
    const token: any = this.jwtHelper.decodeToken(this.getToken());
    const timestamp = +new Date() / 1000;

    if (token && token['exp'] < timestamp) {
      this.clearToken();
      this.router.navigate(['/']);
    }
  }

  private clearUnauthorized(error: any): void {
    if (error instanceof HttpErrorResponse && error.status == 401) {
      this.clearToken();
      this.router.navigate(['/']);
      //this.notify.error('message.unauthorized');
    }
  }

  private getToken(): string {
    return localStorage.getItem('token');
  }

  private clearToken(): void {
    localStorage.removeItem('token');
  }

}
