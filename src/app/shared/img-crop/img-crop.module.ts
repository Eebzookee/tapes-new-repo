import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../../material/material.module';
import { MatCardModule} from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {ImageCropperModule} from 'ngx-image-cropper';
import {TranslateModule} from '@ngx-translate/core';

import {ImgCropComponent} from './img-crop.component';
import {FileDragDropModule} from '../file-drag-drop/file-drag-drop.module';

@NgModule({
  imports: [
    CommonModule,
    ImageCropperModule,
    FlexLayoutModule,
    MaterialModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,

    FileDragDropModule,
    TranslateModule.forChild()
  ],
  declarations: [
    ImgCropComponent
  ],
  exports: [
    ImgCropComponent
  ]
})
export class ImgCropModule {
}
