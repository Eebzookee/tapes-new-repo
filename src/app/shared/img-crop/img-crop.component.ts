import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ImageCroppedEvent} from 'ngx-image-cropper';

@Component({
  selector: 'app-img-crop',
  templateUrl: './img-crop.component.html',
  styleUrls: ['./img-crop.component.scss']
})
export class ImgCropComponent {

  @Output() onChange: EventEmitter<any> = new EventEmitter();
  @Input() resizeToWidth: number = 300;
  @Input() aspectRatio: number = 1;

  cropperVisibility: boolean = false;

  imageChangedEvent: any = '';
  croppedImage: any = '';

  constructor() {
  }

  assignPhoto(event:any): void {
    this.imageChangedEvent = event;
  }

  closeCropper(): void {
    this.imageChangedEvent = null;
    this.onChange.emit(null);
  }

  updateImage(image:any) {
    this.onChange.emit(image);
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.updateImage(this.croppedImage);
  }
}
