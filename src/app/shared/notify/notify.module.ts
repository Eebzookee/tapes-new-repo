import {NgModule} from '@angular/core';
import {ConfirmationService, ConfirmDialogModule, MessageService} from 'primeng/primeng';
import {ToastModule} from 'primeng/toast';
import {TranslateModule} from '@ngx-translate/core';

import {NotifyService} from '~app/shared/notify/notify.service';
import {NotifyComponent} from '~app/shared/notify/notify.component';

@NgModule({
  imports: [
    ConfirmDialogModule,
    ToastModule,
    TranslateModule.forChild()
  ],
  declarations: [NotifyComponent],
  exports: [NotifyComponent],
  providers: [
    NotifyService,
    ConfirmationService,
    MessageService
  ]
})
export class NotifyModule {
}
