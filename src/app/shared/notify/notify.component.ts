import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss']
})
export class NotifyComponent {

  public yesLabel: string = '';
  public noLabel: string = '';
  public header: string = '';

  constructor(
    private translate: TranslateService
  ) {
    this.assignTranslations();
  }

  private assignTranslations(): void {
    this.translate.get('button.yes').subscribe(
      translation => this.yesLabel = translation
    );

    this.translate.get('button.no').subscribe(
      translation => this.noLabel = translation
    );

    this.translate.get('message.confirm_required').subscribe(
      translation => this.header = translation
    );
  }
}
