import {Injectable} from '@angular/core';
//import {ConfirmationService, MessageService} from 'primeng/primeng';
import {Observable, Observer} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

//import {TOAST_TYPE} from '~app/shared/notify/toast-type.const';

@Injectable()
export class NotifyService {

  constructor(
    //private confirmationService: ConfirmationService,
    //private messageService: MessageService,
    private translate: TranslateService
  ) {
  }

  public notify(toastType: string, message: string, title?: string): void {
    /*
    switch (toastType) {
      case TOAST_TYPE.SUCCESS:
        this.messageService.add({severity: 'success', detail: message, summary: title});

        break;

      case TOAST_TYPE.INFO:
        this.messageService.add({severity: 'info', detail: message, summary: title});

        break;

      case TOAST_TYPE.WARNING:
        this.messageService.add({severity: 'warning', detail: message, summary: title});

        break;

      case TOAST_TYPE.ERROR:
        this.messageService.add({severity: 'error', detail: message, summary: title});

        break;
    }
    */
  }

  public success(messageKey: string): void {
    try {
      this.showToast('success', this.translate.instant(messageKey));
    } catch (e) {
      this.showToast('warn', this.translate.instant('message.wrong_translation_key'));
    }
  }

  public error(error: any): void {
    let messageKey: string = error;

    if (typeof error.error === 'string') {
      messageKey = error.error;
    }

    try {
      this.showToast('error', this.translate.instant(messageKey));
    } catch (e) {
      this.showToast('warn', this.translate.instant('message.wrong_translation_key'));
    }
  }

  public confirm(messageKey: string, header?: string): Observable<any> {
    return Observable.create((observer: Observer<boolean>) => {

      /*
      this.confirmationService.confirm({
        message: this.translate.instant(messageKey),
        header,
        accept: () => {
          observer.next(true);
          observer.complete();
        },
        reject: () => {
          observer.error(false);
          observer.complete();
        }
      });
      */
    });
  }

  private showToast(type: string, message: string, title?: string): void {
    //this.messageService.add({severity: type, detail: message, summary: title});
  }
}
