import {Component, Input, OnInit} from '@angular/core';
import {Track} from '../../models/track/track.model';

@Component({
  selector: 'app-track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.scss']
})
export class TrackListComponent implements OnInit {

  @Input() tracks: Track[] = [];
  @Input() hideClose = false;

  constructor() { }

  ngOnInit(): void {
  }

  removeTrackFromProfile(id:any) {
    console.log('removeTrackFromPlaylist if user owns track', id)
  }

}
