import { Component, OnInit } from "@angular/core";
import { AudioService } from "../../services/audio/audio.service";
import { SignupRepository } from "../../services/signup/signup.repository";
import { TrackVersion } from "../../models/track/track.model";
import { UserService } from "../../services/user/user.service";
import { HeaderService } from "../../navigation/header/header.service";
import { ActivatedRoute, Router } from "@angular/router";
import { User } from "../../models/user/user.model";
import { Location } from "@angular/common";

@Component({
  selector: "app-track-upload",
  templateUrl: "./track-upload.component.html",
  styleUrls: ["./track-upload.component.scss"],
})
export class TrackUploadComponent implements OnInit {
  uploadingSong = false;
  trackTitle = "";
  trackDescription = "";
  genre = "Rap";
  uploadProgress = 0;
  versions: { file: File; name: string }[] = [];
  artworkFile: File | null = null;
  artworkBase64 = "";

  versionName = "";
  versionFile: File | null = null;

  allGenres: string[] = [];

  user: User | null = null;
  userId: number | null = null;

  constructor(
    private audioService: AudioService,
    private headerService: HeaderService,
    private signupRepository: SignupRepository,
    private _location: Location,
    public userService: UserService,
    public router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const param = this.route.snapshot.paramMap.get("userId");
    this.userId = param ? Number(param) : null;

    if (this.userId) {
      this.userService.getUser(this.userId).subscribe(
        (response) => {
          this.user = response;
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      this.user = this.userService.user.value!;
    }

    this.audioService.getAllGenres().subscribe(
      (response) => {
        this.allGenres = response.map((genre: { name: string }) => genre.name);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  upload() {
    if (
      !this.trackTitle ||
      !this.trackDescription ||
      !this.genre ||
      !this.artworkBase64
    ) {
      return;
    }

    const data = {
      title: this.trackTitle,
      description: this.trackDescription,
      genre: this.genre,
      cover: this.artworkBase64,
    };

    this.audioService
      .upload(data, this.versions, this.progressOnChange)
      .then((response) => {
        if (response.success) {
          this.uploadingSong = false;

          // check what teh role is before updating it unnecessarily
          if (this.user && this.user.role === "GUEST") {
            this.signupRepository
              .updateUser(this.user)
              .subscribe((response) => {
                this.headerService.changePageLoading("true");

                if (response.error) {
                  console.log("update error", response);

                  if (response.error === "No changes made.") {
                    setTimeout(() => {
                      this.headerService.changePageLoading("false");
                      //this.router.navigateByUrl("/user/profile");
                      this.router.navigate([this.router.url]);
                    }, 3000);
                  }
                } else {
                  setTimeout(() => {
                    this.headerService.changePageLoading("false");
                    //this.router.navigateByUrl("/home");
                    this.router.navigate([this.router.url]);
                  }, 3000);
                }
              });
          } else {
            setTimeout(() => {
              this.headerService.changePageLoading("false");
              //this.router.navigateByUrl("/home");
              this.router.navigate([this.router.url]);
            }, 3000);
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  progressOnChange = (progress: number) => {
    this.uploadProgress = progress;
  };

  versionFileOnChange($event: any) {
    this.versionFile = $event.target.files[0];
  }

  artworkUploadOnChange($event: any) {
    this.artworkBase64 = $event;
  }

  removeVersion(version: { file: File; name: string }) {
    const index = this.versions.indexOf(version);
    if (index >= 0) {
      this.versions.splice(index, 1);
    }
  }

  addVersion() {
    if (this.versionFile && this.versionName) {
      this.versions.push({ file: this.versionFile, name: this.versionName });
      this.versionName = "";
      // Resetting versionFile breaks it for some reason, works fine without
    }
  }
}
