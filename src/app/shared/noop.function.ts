export function noop() {
}

export type VoidFn = () => void;
