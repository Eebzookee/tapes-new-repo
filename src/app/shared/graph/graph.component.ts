import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {TimePeriod, TrackMetric} from '../../services/helpers';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() metrics: TrackMetric[] = [];
  @Input() selectedMetric = 'plays';
  @Input() timePeriod: TimePeriod = TimePeriod.DAY;

  graph: Chart | null = null;

  constructor(
    private elementRef: ElementRef
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!this.graph) { return; }

    const dayNumToPretty = (d: number) => {
      const days = ["M", "T", "W", "T", "F", "S", "S"];
      return days[d];
    }

    console.log(this.metrics);

    const labels = this.metrics.map(t => {
      const date = new Date(t.date);

      console.log("Time Period: ", this.timePeriod)

      switch (this.timePeriod) {
        case TimePeriod.DAY:
          return date.toLocaleTimeString();
        case TimePeriod.WEEK:
          console.log("DATE: ", date, "DAY: ", date.getDay());
          return dayNumToPretty(date.getDay());
        case TimePeriod.MONTH:
          return date.getDate();
    }});

    const data = this.metrics.map(t => {
      switch (this.selectedMetric.toLowerCase()) {
        case 'plays':
          return t.plays;
        case 'likes':
          return t.likes;
        case 'downloads':
          return t.downloads;
        case 'comments':
          return t.comments;
        default:
          return t.plays;
      }
    });

    this.graph.data.labels = labels;
    if (this.graph.data.datasets) {
      this.graph.data.datasets[0].data = data;
    }

    this.graph.update();
  }

  ngAfterViewInit() {
    this.initGraph();
  }

  initGraph() {
    const element = this.elementRef.nativeElement.querySelector('#graph');

    if (element) {
      this.graph = new Chart(element, {
        type: 'line',
        data: {
          labels: [],
          datasets: [{
            label: 'Metrics',
            data: [],
            backgroundColor: "#8D5EEE",
            borderColor: "#ddcffa",
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            xAxes: [{
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero: true
              },
            }]
          }
        }
      });
    }
  }
}
