import {Component, DoCheck, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'error-display',
  templateUrl: './error-display.component.html',
  styleUrls: ['./error-display.component.scss']
})
export class ErrorDisplayComponent implements DoCheck {

  @Input()
  public errors: any = null;
  @Input()
  public visibility: boolean = false;

  public errorMessages: string[] = [];

  constructor(
    private translate: TranslateService
  ) {
  }

  ngDoCheck(): void {
    this.checkErrors();
  }

  private checkErrors(): void {
    this.errorMessages = [];

    if (!this.errors || !this.visibility) {
      return;
    }

    for (const errorKey in this.errors) {
      if (!this.errors.hasOwnProperty(errorKey)) {
        continue;
      }

      this.setError(errorKey, this.errors[errorKey]);
    }
  }

  private setError(errorKey: string, params?: any): void {
    const matchingParams: string[] = [
      'min',
      'max',
      'requiredPattern',
      'requiredLength'
    ];

    for (const errorParamKey in params) {
      if (!params.hasOwnProperty(errorParamKey)) {
        continue;
      }

      if (!matchingParams.includes(errorParamKey)) {
        continue;
      }

      this.errorMessages.push(this.translate.instant('error.' + errorKey, {param: params[errorParamKey]}));
    }

    if (typeof params !== 'object') {
      this.errorMessages.push(this.translate.instant('error.' + errorKey));
    }
  }
}
