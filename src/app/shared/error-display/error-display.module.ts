import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ErrorDisplayComponent} from '~app/shared/error-display/error-display.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ErrorDisplayComponent
  ],
  exports: [
    ErrorDisplayComponent
  ]
})
export class ErrorDisplayModule {
}
