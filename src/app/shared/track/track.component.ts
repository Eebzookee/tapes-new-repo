import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Track} from '../../models/track/track.model';
import { AudioService } from '../../services/audio/audio.service';
import {UserService} from '../../services/user/user.service';
import {User} from '../../models/user/user.model';
import {TrackPopupService} from '../../services/track-popup/track-popup.service';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {

  @Input() track: Track | null = null;
  @Input() onCloseClickCallback: () => void = () => {};
  //@Input() removeTrack: (id:any) => void = () => {};
  @Input() hideClose = false;
  @Input() showPopupButton = true;

  user: User | null = null;

  constructor(
    private audioService: AudioService,
    public userService: UserService,
    private trackPopupService: TrackPopupService
  ) {

  }

  ngOnInit(): void {

  }

  popupButtonOnClick() {
    this.trackPopupService.track.next(this.track);
  }

  playtrack(track:any) {
    this.audioService.changeTrack(track);
  }

  removeTrackFromProfile(track:Track) {
    this.user = this.userService.user.value;
    if( this.user && this.user.id === track.user.id) {
      //  use service to change track listing
      console.log('use service to change track listing')

    }

  }

}
