import {Component, Input, OnInit} from '@angular/core';
import {Feedback} from '../../models/feedback/feedback.model';
import {UserService} from '../../services/user/user.service';
import {AdminService} from '../../services/admin/admin.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

  @Input() feedback: Feedback | null = null;
  status = 'remove';
  removed = false;


  constructor(
    public userService: UserService,
    private adminService: AdminService
  ) { }

  ngOnInit(): void {
    this.status = this.feedback?.givenToken ? 'add' : 'remove';

  }

  removeFeedback() {
    if (this.feedback) {
      this.adminService.removeFeedback(this.feedback.commentId).subscribe(() => {
        this.removed = true;
      }, error => {
        console.log(error);
      });
    }
  }

  handleStatusChange(event: string) {
    if (event === 'add') {
      this.giveToken();
    } else {
      this.removeToken();
    }
  }

  giveToken() {
    if (this.feedback) {
      this.adminService.giveToken(this.feedback.commentId, this.feedback.userId).subscribe(() => {
        if (this.feedback) {
          this.feedback.givenToken = true;
        }
      }, error => {
        console.log(error);
      });
    }
  }

  removeToken() {
    if (this.feedback) {
      this.adminService.removeToken(this.feedback.commentId).subscribe(() => {
        if (this.feedback) {
          this.feedback.givenToken = false;
        }
      }, error => {
        console.log(error);
      });
    }
  }

}
