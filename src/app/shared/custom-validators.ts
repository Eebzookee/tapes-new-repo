import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function equalValidator(ctrl: AbstractControl): ValidatorFn {
  return function (control: AbstractControl): ValidationErrors {
    if (ctrl.value === control.value) {
      return null;
    }

    return {
      match: false
    };
  };

}

export function passwordValidator(ctrl: AbstractControl): ValidatorFn {
  return function (): ValidationErrors {
    const regexp: RegExp = RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/);

    if (regexp.test(ctrl.value)) {
      return null;
    }

    return {
      password: false
    };
  }
}

export function letterValidator(ctrl: AbstractControl): ValidatorFn {
  return function (): ValidationErrors {
    const regexp: RegExp = RegExp(/^[\s\p{L}]+$/u);

    if (regexp.test(ctrl.value)) {
      return null;
    }

    return {
      letter: false
    };
  }
}

export function objectValidator(ctrl: AbstractControl): ValidatorFn {
  return function (): ValidationErrors {
    if (ctrl.value !== null && typeof ctrl.value === 'object') {
      return null;
    }

    return {
      is_not_object: false
    };
  }
}
