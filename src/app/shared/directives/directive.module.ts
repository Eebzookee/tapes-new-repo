import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ErrorDirective} from './error.directive';
import {ErrorDisplayComponent} from '../error-display/error-display.component';
import {ErrorDisplayModule} from '../error-display/error-display.module';
import {LabelDirective} from './label.directive';

@NgModule({
  declarations: [
    ErrorDirective,
    LabelDirective
  ],
  exports: [
    ErrorDirective,
    LabelDirective
  ],
  imports: [
    CommonModule,
    ErrorDisplayModule
  ],
  entryComponents: [
    ErrorDisplayComponent
  ]
})
export class DirectiveModule {
}
