import {Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appLabel]'
})
export class LabelDirective {

  @Input()
  private appLabel: string;

  constructor(
    private renderer: Renderer2,
    private elementRef: ElementRef
  ) {
  }

  ngOnInit() {
    if (this.appLabel) {
      this.wrap();
    }
  }

  private wrap(): void {
    const wrapper = this.renderer.createElement('div');
    wrapper.className = "ui-float-label";

    const label = this.renderer.createElement('label');
    const text = this.renderer.createText(this.appLabel);
    this.renderer.appendChild(label, text);

    const el = this.elementRef.nativeElement;
    const parent = el.parentNode;
    this.renderer.insertBefore(parent, wrapper, el);

    this.renderer.appendChild(wrapper, el);
    this.renderer.appendChild(wrapper, label);
  }

}
