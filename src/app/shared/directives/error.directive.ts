import {Directive, ElementRef, Input, Renderer2} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Directive({
  selector: '[appError]'
})
export class ErrorDirective {

  @Input()
  private appError: any;

  private errorMessages: string[] = [];
  private errorContainer: HTMLElement;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    this.wrap();
  }

  ngOnChanges(): void {
    this.checkErrors();
    this.assignError();
  }

  private wrap(): void {
    let wrapper = this.renderer.createElement('div');
    wrapper.style.position = "relative";

    let errorContainer = document.createElement('div');
    errorContainer.className = "app-error";

    const el = this.elementRef.nativeElement;
    const parent = el.parentNode;

    this.renderer.insertBefore(parent, wrapper, el);

    this.renderer.appendChild(wrapper, el);
    this.renderer.appendChild(wrapper, errorContainer);

    this.errorContainer = errorContainer;
  }

  private assignError(): void {
    if (this.errorContainer) {
      this.errorContainer.innerHTML = this.errorMessages[0] || '';
    }
  }

  private checkErrors(): void {
    this.errorMessages = [];

    if (!this.appError) {
      return;
    }

    for (const errorKey in this.appError) {
      if (!this.appError.hasOwnProperty(errorKey)) {
        continue;
      }

      this.setError(errorKey, this.appError[errorKey]);
    }
  }

  private setError(errorKey: string, params?: any): void {
    const matchingParams: string[] = [
      'min',
      'max',
      'requiredPattern',
      'requiredLength'
    ];

    for (const errorParamKey in params) {
      if (!params.hasOwnProperty(errorParamKey)) {
        continue;
      }

      if (!matchingParams.includes(errorParamKey)) {
        continue;
      }

      this.errorMessages.push(this.translate.instant('error.' + errorKey, {param: params[errorParamKey]}));
    }

    if (typeof params !== 'object') {
      this.errorMessages.push(this.translate.instant('error.' + errorKey));
    }

  }
}
