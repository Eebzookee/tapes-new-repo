import { Component, Input, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';
import {GlobalMetric, UserStatsTimePeriod} from 'src/app/services/helpers';

@Component({
  selector: 'app-user-stats',
  templateUrl: './user-stats.component.html',
  styleUrls: ['./user-stats.component.scss']
})
export class UserStatsComponent implements OnInit {

  @Input() timePeriod = UserStatsTimePeriod.WEEK; // TOTAL or WEEK

  metrics: GlobalMetric | null = null;

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit(): void {
    this.dashboardService.getMyGlobalMetrics(this.timePeriod).subscribe(response => {
      this.metrics = response;
    }, error => console.log(error));
  }

}
