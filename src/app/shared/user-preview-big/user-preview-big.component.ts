import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user/user.model';

@Component({
  selector: 'app-user-preview-big',
  templateUrl: './user-preview-big.component.html',
  styleUrls: ['./user-preview-big.component.scss']
})
export class UserPreviewBigComponent implements OnInit {

  @Input() user: User | null = null;

  constructor() { }

  ngOnInit(): void {
  }

}
