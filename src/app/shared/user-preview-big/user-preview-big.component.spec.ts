import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPreviewBigComponent } from './user-preview-big.component';

describe('ArtistPreviewBigComponent', () => {
  let component: UserPreviewBigComponent;
  let fixture: ComponentFixture<UserPreviewBigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPreviewBigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPreviewBigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
