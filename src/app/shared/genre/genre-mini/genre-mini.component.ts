import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-genre-mini',
  templateUrl: './genre-mini.component.html',
  styleUrls: ['./genre-mini.component.scss']
})
export class GenreMiniComponent implements OnInit {

  @Input() genre = '';

  constructor() { }

  ngOnInit(): void {
  }

}
