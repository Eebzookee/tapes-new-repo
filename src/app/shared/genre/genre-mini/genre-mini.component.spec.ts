import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenreMiniComponent } from './genre-mini.component';

describe('MiniComponent', () => {
  let component: GenreMiniComponent;
  let fixture: ComponentFixture<GenreMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenreMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenreMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
