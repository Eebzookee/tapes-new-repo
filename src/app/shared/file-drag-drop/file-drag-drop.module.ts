import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileDropModule} from 'ngx-file-drop';

import {FileDragDropComponent} from './file-drag-drop.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  exports: [FileDragDropComponent],
  declarations: [FileDragDropComponent],
  imports: [
    CommonModule,
    FileDropModule,
    TranslateModule.forChild()
  ]
})
export class FileDragDropModule {
}
