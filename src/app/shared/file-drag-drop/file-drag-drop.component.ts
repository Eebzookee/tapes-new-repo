import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FileSystemFileEntry, UploadEvent, UploadFile} from 'ngx-file-drop';

@Component({
  selector: 'app-file-drag-drop',
  templateUrl: './file-drag-drop.component.html',
  styleUrls: ['./file-drag-drop.component.scss']
})
export class FileDragDropComponent implements OnInit {

  @Input()
  public icon!: string;
  @Input()
  responseType!: string;
  @Input()
  accept: string = '';
  files:any = [] //UploadFile[] = [];
  @Output()
  private onChange: EventEmitter<File> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  public fileChangeEvent(event:any) {
    this.files = event.target.files;
    console.log('this.files', this.files)
    let file = event.target.files[0];
    console.log('file', file);

    this.onChange.emit(file);
  }

  public dropped(event: UploadEvent) {
    this.files = event.files;

    for (const droppedFile of event.files) {
      if (!droppedFile.fileEntry.isFile) {
        return;
      }

      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;

      fileEntry.file(
        (file: File) => {
          this.onChange.emit(file);
        }
      );

      break;
    }
  }

}
