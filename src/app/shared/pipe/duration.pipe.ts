import {Pipe, PipeTransform} from "@angular/core";
import * as dateFns from 'date-fns';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  constructor() {
  }

  transform(value: number): string {
    const sec: number = Math.floor(value);
    let format: string = 'hh:mm:ss'

    if (sec < 3600) {
      format = 'mm:ss'
    }

    var helperDate = dateFns.addSeconds(new Date(0), sec);

    return dateFns.format(helperDate, format);

  }
}
