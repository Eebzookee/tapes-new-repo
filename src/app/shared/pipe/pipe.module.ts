import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NumberToArrayPipe} from './number-to-array.pipe';
import {SanitizePipe} from './sanitize.pipe';
import {DurationPipe} from './duration.pipe';

@NgModule({
  declarations: [
    NumberToArrayPipe,
    SanitizePipe,
    DurationPipe
  ],
  exports: [
    NumberToArrayPipe,
    SanitizePipe,
    DurationPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipeModule {

  static forRoot() {
    return {
      ngModule: PipeModule,
      providers: []
    }
  }
}
