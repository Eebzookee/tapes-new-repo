import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'numberToArray'
})
export class NumberToArrayPipe implements PipeTransform {

  transform(value): any {
    let res = [];
    let start = 0;
    let end = value;

    if (Array.isArray(value)) {
      start = value[0];
      end = value[1];
    }

    for (start; start < end; start++) {
      res.push(start);
    }

    return res;

  }

}
