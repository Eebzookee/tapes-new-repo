export class SelectItem<T> {
  constructor(
    public label: string,
    public value: T
  ) {
  }
}

export class SelectItemMapper {
  public static map<T>(label: string, value: T): SelectItem<T> {
    if (!label) {
      return;
    }

    return new SelectItem<T>(
      label,
      value
    )

  }

}
