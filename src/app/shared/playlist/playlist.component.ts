import { Component, Input, OnInit } from '@angular/core';
import { Playlist } from 'src/app/models/playlist/playlist.model';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent implements OnInit {

  @Input() playlist: Playlist | null = null;
  @Input() showWithZeroTracks = false;

  constructor() { }

  ngOnInit(): void {
  }

}
