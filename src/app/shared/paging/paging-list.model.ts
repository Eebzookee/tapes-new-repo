export class PagingList<T> {

  constructor(
    public content: T[] = [],
    public totalElements: number = 0,
    public size: number = 0
  ) {
  }

}
