import {PagingList} from "~app/shared/paging/paging-list.model";

export class PagingMapper {

  public static map<T>(data: any, list: T[]): PagingList<T> {
    if (!data) {
      return null;
    }

    return new PagingList<T>(
      list,
      data.totalElements,
      data.size
    );
  }
}
