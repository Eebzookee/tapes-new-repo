import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Location } from "@angular/common";
import { HeaderService } from "../navigation/header/header.service";

@Component({
  selector: 'app-page5',
  templateUrl: './page5.component.html',
  styleUrls: ['./page5.component.scss']
})
export class Page5Component implements OnInit {

  constructor(
    private headerService: HeaderService,
    private _location: Location
  ) {}

  ngOnInit(): void {
    this.headerService.changePageType("withmenu");
    this.headerService.changePageTitle("page 5");    
  }

  backClicked() {
    this._location.back();
  }

}
