import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import jwtDecode from 'jwt-decode';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoutingModule } from './routing/routing.module';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { NguCarouselModule } from '@ngu/carousel';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { NgxPermissionsModule, NgxPermissionsService } from 'ngx-permissions';

import { MaterialModule } from './material/material.module';
import { MatCardModule} from '@angular/material/card';
import { MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';

import { AppComponent } from './app.component';
import { HeaderComponent } from './navigation/header/header.component';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './user/login/login.component';
import { ProfilePageComponent } from './user/profile-page/profile-page.component';
import { GenreMiniComponent } from './shared/genre/genre-mini/genre-mini.component';
import { RegisterComponent } from './user/register/register.component';
import { SignupUserDetailsStepComponent } from './steps/signup-user-details-step/signup-user-details-step.component';
import { SignupGenresStepComponent } from './steps/signup-genres-step/signup-genres-step.component';

import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { Page4Component } from './page4/page4.component';
import { Page5Component } from './page5/page5.component';
import { Page6Component } from './page6/page6.component';
import { Page7Component } from './page7/page7.component';
import { Page8Component } from './page8/page8.component';
import { Page9Component } from './page9/page9.component';
import { Page10Component } from './page10/page10.component';
import { Page25Component } from './page25/page25.component';

import { UserService } from './services/user/user.service';
import { SignupRepository } from './services/signup/signup.repository';
import {ImgCropModule} from './shared';
import { TrackComponent } from './shared/track/track.component';
import { TrackSearchComponent } from './shared/track-search/track-search.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { UserStatsComponent } from './shared/user-stats/user-stats.component';
import { SignupGigsStepComponent } from './steps/signup-gigs-step/signup-gigs-step.component';
import { SignupPackageDetailsStepComponent } from './steps/signup-package-details-step/signup-package-details-step.component';
import { WelcomeComponent } from './user/welcome/welcome.component';
import { FeaturedArtistComponent } from './featured-artist/featured-artist.component';
import { FeaturedSongsComponent } from './featured-songs/featured-songs.component';
import {AuthInterceptor} from './auth.interceptor';
import { TrackListComponent } from './shared/track-list/track-list.component';
import { UserPreviewBigComponent } from './shared/user-preview-big/user-preview-big.component';
import { GraphComponent } from './shared/graph/graph.component';
import { AudioPlayerComponent, DialogOverviewExampleDialog } from './audio/audio-player/audio-player.component';
import { TrackTimeDisplayPipe } from './audio/pipes/track-time-display.pipe';
import { MetricWidgetComponent } from './shared/metric-widget/metric-widget.component';
import { LibraryComponent } from './user/library/library.component';
import { LikedTracksComponent } from './user/library/liked-tracks/liked-tracks.component';
import { PlaylistsComponent } from './user/library/playlists/playlists.component';
import { FollowingComponent } from './user/library/following/following.component';
import { PlaylistComponent } from './shared/playlist/playlist.component';
import { UserPreviewComponent } from './shared/user-preview/user-preview.component';
import {UserMapper} from './models/user/user.model';
import { TrackUploadComponent } from './shared/track-upload/track-upload.component';
import { DjProfilePageComponent } from './user/dj-profile-page/dj-profile-page.component';
import { FeedbackComponent } from './shared/feedback/feedback.component';
import {MatChipsModule} from '@angular/material/chips';
import { ViewUsersComponent } from './admin/view-users/view-users.component';
import {MatTableModule} from '@angular/material/table';
import { SearchComponent } from './search/search.component';
import { TrackPopupComponent } from './global/track-popup/track-popup.component';
import { TrackCommentsComponent } from './global/track-popup/track-comments/track-comments.component';
import { AddToPlaylistComponent } from './global/track-popup/add-to-playlist/add-to-playlist.component';
import { CreatePlaylistDialogComponent } from './global/track-popup/add-to-playlist/create-playlist-dialog/create-playlist-dialog.component';
import { AddCommentDialogComponent } from './global/track-popup/track-comments/add-comment-dialog/add-comment-dialog.component';
import { DownloadComponent } from './global/track-popup/download/download.component';
import { UpgradeComponent } from './user/upgrade/upgrade.component';
import { BankDetailsComponent } from './user/bank-details/bank-details.component';


export function initConfiguration(userService: UserService, permissionsService: NgxPermissionsService) {
  return () => {
    const token = localStorage.getItem('token'); // TODO : replace with cookie
    if (token) {
        const user = UserMapper(jwtDecode(token));
        userService.user.next(user);
        permissionsService.loadPermissions([user.role || 'USER']);
    } else {
        permissionsService.loadPermissions(['GUEST']);
    }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    HeaderComponent,
    SidenavListComponent,
    Page1Component,
    Page2Component,
    Page3Component,
    Page4Component,
    Page5Component,
    Page6Component,
    Page7Component,
    Page8Component,
    Page9Component,
    Page10Component,
    ProfilePageComponent,
    GenreMiniComponent,
    Page25Component,
    LoginComponent,
    RegisterComponent,
    SignupUserDetailsStepComponent,
    SignupGenresStepComponent,
    TrackComponent,
    TrackSearchComponent,
    DashboardComponent,
    UserStatsComponent,
    SignupGigsStepComponent,
    SignupPackageDetailsStepComponent,
    WelcomeComponent,
    FeaturedArtistComponent,
    FeaturedSongsComponent,
    TrackListComponent,
    UserPreviewBigComponent,
    GraphComponent,
    AudioPlayerComponent,
    DialogOverviewExampleDialog,
    TrackTimeDisplayPipe,
    MetricWidgetComponent,
    LibraryComponent,
    LikedTracksComponent,
    PlaylistsComponent,
    FollowingComponent,
    PlaylistComponent,
    UserPreviewComponent,
    TrackUploadComponent,
    DjProfilePageComponent,
    FeedbackComponent,
    ViewUsersComponent,
    SearchComponent,
    TrackPopupComponent,
    TrackCommentsComponent,
    AddToPlaylistComponent,
    CreatePlaylistDialogComponent,
    AddCommentDialogComponent,
    DownloadComponent,
    UpgradeComponent,
    BankDetailsComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxPermissionsModule.forRoot(),
    MaterialModule,
    FlexLayoutModule,
    RoutingModule,
    NguCarouselModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    ImgCropModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatDialogModule,
    MatChipsModule,
    MatTableModule
  ],
  providers: [
    UserService,
    NgxPermissionsService,
    SignupRepository,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfiguration,
      multi: true,
      deps: [UserService, NgxPermissionsService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
