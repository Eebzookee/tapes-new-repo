import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import jwtDecode from 'jwt-decode';
import { Router } from '@angular/router';
import {User, UserMapper} from 'src/app/models/user/user.model';
import { NgxPermissionsService } from 'ngx-permissions';
import { Location } from '@angular/common';
import { HeaderService } from '../../navigation/header/header.service';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email = '';
  password = '';
  error = '';

  constructor(
    private userService: UserService,
    private router: Router,
    private permissionsService: NgxPermissionsService,
    private headerService: HeaderService,
    private _location: Location,
  ) { }

  ngOnInit(): void {
    this.headerService.changePageType('withmenu');
    this.headerService.changePageTitle('');
  }

  async onLoginClick() {
    try {
      const response = await this.userService.attemptUserLogin(this.email, this.password).toPromise();
      // TODO: Store bearer more securely
      localStorage.setItem('token', response);

      const user = UserMapper(jwtDecode(response));
      this.userService.user.next(user);
      this.permissionsService.loadPermissions([user.role]);

      this.router.navigateByUrl('/user/profile');

    } catch (error) {
      console.log('error', error)

      switch (error.error) {
        case 'message.user_doesnt_exists':
          return this.error = 'user does not exist'
        default:
          return this.error = 'an error has occurred, please try again'
      }


      this.error = error;
    }
  }
}
