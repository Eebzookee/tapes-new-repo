import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HeaderService } from "../../navigation/header/header.service";
import { RevenueService } from 'src/app/services/revenue/revenue.service';
import { User } from "../../models/user/user.model";

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.scss']
})

export class BankDetailsComponent implements OnInit {

  @Input() user!: User;
  bankFormGroup!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private headerService: HeaderService,
    private revenueService: RevenueService,
  ) { }

  ngOnInit(): void {
    
    this.bankFormGroup = this.formBuilder.group({
      BankName: ["", Validators.required],
      BankAccountNumber: ["", Validators.required],
      BankSortCode: ["", Validators.required],
      BankAccountHolderName: ["", Validators.required],
      RecipientEmailAddress: ["", Validators.required],
    });
  
  }

  async bankAddRecipient() {
    console.log('bankAddRecipient')
    this.headerService.changePageLoading("true");
    let formData = this.bankFormGroup.value;
    formData["ConsumerID"] = this.user!.id;
    formData["BankAccountTypeID"] = 1;

    this.revenueService.bankAddRecipient(formData).subscribe(response => {
      if (response.error) {
        console.log('bank  add recipient error')
        this.headerService.changePageLoading("false");
      } else {        
        setTimeout(() => {
          this.headerService.changePageLoading("false");
          //this.router.navigateByUrl("/user/profile");
          /**
           * so....ordered new cammpaign this gives use 1 upload allowed alon with whatever uploads they've already had
           * 
           */
        }, 1000);
      }
    });
    
  }

/*

{
  "ConsumerID": 55320,
  "BankName": "AddBankC1",
  "BankAddress": "New Bank AddressC1",
  "BankAccountNumber": "00709983",
  "BankSortCode": "623053",
  "BankAccountHolderName": "Bank AccHolderC1",
  "ReferenceNumber": "AddBankRefC1",
  "FriendlyName": "BankNameC1",
  "BankAccountTypeID": 1,
  "RecipientISOCountryCode": "826",
  "IBAN": "GB35CNFV60837000000570",
  "BIC": "CNFVGB21XXX",
  "NCC": "",
  "BankAccountHolderAddress": "ACH AddressC1",
  "RecipientEmailAddress": "abc.xyz@ol.com",
  "BeneficiaryCity": "Bruxelles",
  "BeneficiaryState": "Bruxelles",
  "BeneficiaryPostCode": "1000",
  "BankEntityTypeID": 1,
  "CLABE": "032180000118359719",
  "ABA": "091000022",
  "IFSC": "HDFC0003779",
  "BsbCode": "012002",
  "BankCode": "001",
  "BranchCode": "91108",
  "CNAPS": "104100000004",
  "TransferTypeID": 21,
  "ClientRequestReference": "RequestReference1",
  "CultureID": 1
}

*/

}
