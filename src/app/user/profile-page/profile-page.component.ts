import { Component, OnInit } from '@angular/core';
import { AudioService } from 'src/app/services/audio/audio.service';
import { Location } from '@angular/common';
import { HeaderService } from 'src/app/navigation/header/header.service';
import {UserService} from '../../services/user/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../models/user/user.model';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  fileToUpload: File | null = null;
  progress = 0;
  sortByOptions: any = [];
  user: User | null = null;
  userId: number | null = null;
  artists: User[] = [];
  me = false;
  canupload:boolean = false;

  constructor
 (
    private audioService: AudioService,
    private headerService: HeaderService,
    private _location: Location,
    public userService: UserService,
    public router: Router,
    private route: ActivatedRoute
    // private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    window.scroll(0,0);
    this.headerService.changePageType('withmenu');

    this.sortByOptions = [
      {label: 'Likes', value: 'num_likes'},
      {label: 'Comments', value: 'num_comments'},
      {label: 'Downloads', value: 'num_downloads'},
      {label: 'Shares', value: 'num_shares'},
      {label: 'Plays', value: 'num_plays'},
    ];

    const param = this.route.snapshot.paramMap.get('userId');
    this.userId = param ? Number(param) : null;
    this.me = !this.userId;


    this.headerService.changePageTitle(this.userId ? '|Profile' : 'My|profile');

    if (this.userId) {
      this.userService.getUser(this.userId).subscribe(response => {
        this.user = response;
        console.log('userId already exists', this.user)
        this.numOfTracks(this.user)

      }, error => {
        console.log(error);
      });
    } else {
      this.user = this.userService.user.value!;
      console.log('user from service', this.user)
      this.getArtists()


      
      this.numOfTracks(this.user)
    }

  }

  getArtists() {
    this.userService.getFollowedArtists().subscribe(response => {
      this.artists = response;
    }, err => {
      console.log(err);
    })
  }

  numOfTracks(user:any) {
    this.audioService.getTracks({
      userId: user.id
    }).subscribe(tracks => {
      console.log('total tracks', tracks)
      // decide wether to use profiles package cacn allow for more uploads
      this.canupload = (tracks.length < 1) ? true : false;




    }, error => {
      console.log(error);
    });
  }

  backClicked() {
    this._location.back();
  }
}
