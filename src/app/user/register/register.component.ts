import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { speshCharValidator } from "../../validators/custom.validators";
import { Location } from "@angular/common";
import { HeaderService } from "../../navigation/header/header.service";
import { SignupRepository } from "../../services/signup/signup.repository";
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup!: FormGroup;
  inviteCode!: string;
  usertype: string = '';
  usertypeSelect: string = '';
  public dj:boolean = false;
  public artist:boolean = false;
  public starter:boolean = true;
  public usertypeSelected:boolean = false;

  constructor(
    private headerService: HeaderService,
    private _location: Location,
    private formBuilder: FormBuilder,
    private signupRepository: SignupRepository,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.headerService.changePageType("withnomenu");
    this.headerService.changePageTitle("Register");
    
    this.formGroup = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      city: ["", Validators.required],
      instagram: ["", Validators.required],
      username: ["", Validators.required],
      web: [""],
      twitter: [""],
      profilePicture: ["", Validators.required],
      bio: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(8), speshCharValidator]],
      passwordRepeat: ["", Validators.required],
      genres: [[]],
      radiotext:[""],
      podcaststext:[""],
      clubstext:[""],
      othertext:[""],
      radioStations: this.formBuilder.array([]),
      clubs: this.formBuilder.array([]),
      other: this.formBuilder.array([]),
      podcasts: this.formBuilder.array([]),
      package: [""],
      sortCode:[""],
      accountNumber:[""],
      inviteCode:[""]
    });
  }


  public checkError = (controlName: string, errorName: string) => {
    console.log('controlName', controlName);
    console.log('errorName', errorName);
    return this.formGroup.controls[controlName].hasError(errorName);

  }

  chooseUserType(usertype:string){
    this.starter = false;
    this.dj = false;
    this.artist = false;
    usertype === 'dj' ? (this.dj = true, this.artist = false) : (this.artist = true, this.dj = false);
    this.usertypeSelect = usertype;
    this.usertypeSelected = true;
  }

  startRegistry() {
    this.usertype = this.usertypeSelect;
  }

  yourDetailsCompleted() {
    return this.formGroup.controls["name"].valid &&
      this.formGroup.controls["username"].valid &&
      this.formGroup.controls["email"].valid &&
      this.formGroup.controls["city"].valid &&
      this.formGroup.controls["instagram"].valid && 
      this.formGroup.controls["profilePicture"].valid &&
      this.passwordCompleted();
  }

  yourGenresCompleted() {
    return this.formGroup.controls["genres"].valid;
  }

  yourProfileCompleted() {
    return this.formGroup.controls["bio"].valid &&
      this.formGroup.controls["profilePicture"].valid;
  }

  passwordCompleted() {
    return this.formGroup.controls["password"].valid &&
      this.formGroup.controls["passwordRepeat"].valid &&
      this.formGroup.controls["password"].value === this.formGroup.controls["passwordRepeat"].value;
  }

  whereYouPlayCompleted() {
    return this.formGroup.controls["other"].value.length > 0 ||
      this.formGroup.controls["radioStations"].value.length > 0 ||
      this.formGroup.controls["podcasts"].value.length > 0 ||
      this.formGroup.controls["clubs"].value.length > 0
  }

  genresCompleted() {
    return this.formGroup.controls["genres"].value.length > 0;
  }

  packageCompleted() { 
    return this.formGroup.controls["package"].value !== ""
  }

  async completeSignup() {
    console.log('completeSignup');
    
    //this.loadingService.setLoading(true);
    this.headerService.changePageLoading("true");

    let formData = this.formGroup.value;
    formData["type"] = (this.dj) ? "DJ" : "Artist"
    //formData["type"] = "DJ";
    formData["inviteCode"] = this.inviteCode;

    //fix gig data
    formData["radioStations"] = formData["radioStations"].map((gig:any) => {
      return gig.gig
    })
    formData["podcasts"] = formData["podcasts"].map((gig:any) => {
      return gig.gig
    })
    formData["clubs"] = formData["clubs"].map((gig:any) => {
      return gig.gig
    })
    formData["other"] = formData["other"].map((gig:any) => {
      return gig.gig
    })

    console.log('formData', formData)

    this.signupRepository.register(formData).subscribe(response => {
      if (response.error) {
        //this.notifyService.error(response.error);
        console.log('sign up error')
      } else {
        //this.notifyService.success("message.success");
        
        setTimeout(() => {
          this.headerService.changePageLoading("false");
          this.router.navigateByUrl("/user/login");
        }, 1000);
        
        //this.authService.openAuthDialog("login");
      }
      //this.loadingService.setLoading(false);
    });
    
  }

}
