import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HeaderService } from "../../navigation/header/header.service";
import { SignupRepository } from "../../services/signup/signup.repository";
import { Router } from '@angular/router';

@Component({
  selector: 'app-upgrade',
  templateUrl: './upgrade.component.html',
  styleUrls: ['./upgrade.component.scss']
})
export class UpgradeComponent implements OnInit {
  formGroup!: FormGroup;

  constructor(
    private headerService: HeaderService,
    private formBuilder: FormBuilder,
    private signupRepository: SignupRepository,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.headerService.changePageType("withnomenu");
    this.headerService.changePageTitle("Upgrade");


    this.formGroup = this.formBuilder.group({
      package: [""],
    });
    window.scroll(0,0);


  }

  packageCompleted() { 
    return this.formGroup.controls["package"].value !== ""
  }

}
