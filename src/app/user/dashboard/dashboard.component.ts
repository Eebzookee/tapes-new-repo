import {Component, ElementRef, OnInit} from '@angular/core';
import {HeaderService} from '../../navigation/header/header.service';
import * as Chart from 'chart.js';
import {MetricType, TimePeriod, TrackMetric} from '../../services/helpers';
import {DashboardService} from '../../services/dashboard/dashboard.service';
import { Track } from 'src/app/models/track/track.model';
import { AudioService } from 'src/app/services/audio/audio.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  metricsForTodayByTrack: TrackMetric[] = [];
  metricsForThisWeekByTrack: TrackMetric[] = [];
  metricsForThisMonthByTrack: TrackMetric[] = [];
  metricsForTodayByLocation: TrackMetric[] = [];
  metricsForThisWeekByLocation: TrackMetric[] = [];
  metricsForThisMonthByLocation: TrackMetric[] = [];

  selectedMetric = '';
  selectedTrack = -1;
  selectedLocation = 'GB';

  myTracks: Track[] = [];
  popularTracks: Track[] = [];

  constructor(
    private headerService: HeaderService,
    private elementRef: ElementRef,
    private dashboardService: DashboardService,
    private audioService: AudioService
  ) { }

  // TODO: all of the console.logs for errors in here need to be changed to toats
  // or somethingelse
  ngOnInit(): void {
    this.headerService.changePageType("withnomenu");
    this.headerService.changePageTitle('My|Data');
    this.fetchData();
  }

  fetchData(): void {
    // TODO: this needs to be changed so that it gets your tracks not everyones.
    this.audioService.getTracks({me: true}).subscribe(response => {
      this.myTracks = response;
      this.popularTracks = this.myTracks
        .sort((a, b) => b.metrics.numberPlays.value - a.metrics.numberPlays.value)
        .slice(0, 3);

    }, error => {
      console.log(error);
    });

    this.dashboardService.getMetrics(TimePeriod.DAY, MetricType.TRACK, this.selectedTrack, this.selectedLocation)
      .subscribe(response => {
      this.metricsForTodayByTrack = response;
    }, error => {
      console.log(error);
    });

    this.dashboardService.getMetrics(TimePeriod.WEEK, MetricType.TRACK, this.selectedTrack, this.selectedLocation)
      .subscribe(response => {
      this.metricsForThisWeekByTrack = response;
    }, error => {
      console.log(error);
    });

    this.dashboardService.getMetrics(TimePeriod.MONTH, MetricType.TRACK, this.selectedTrack, this.selectedLocation)
      .subscribe(response => {
      this.metricsForThisMonthByTrack = response;
    }, error => {
      console.log(error);
    });

    this.dashboardService.getMetrics(TimePeriod.DAY, MetricType.LOCATION, this.selectedTrack, this.selectedLocation)
      .subscribe(response => {
      this.metricsForTodayByLocation = response;
    }, error => {
      console.log(error);
    });

    this.dashboardService.getMetrics(TimePeriod.WEEK, MetricType.LOCATION, this.selectedTrack, this.selectedLocation)
      .subscribe(response => {
      this.metricsForThisWeekByLocation = response;
    }, error => {
      console.log(error);
    });

    this.dashboardService.getMetrics(TimePeriod.MONTH, MetricType.LOCATION, this.selectedTrack, this.selectedLocation)
      .subscribe(response => {
      this.metricsForThisMonthByLocation = response;
    }, error => {
      console.log(error);
    });
  }

  initChart(selector: string): Chart | null {
    const element = this.elementRef.nativeElement.querySelector(selector);

    if (element) {
      return new Chart(element, {
        type: 'bar',
        data: {
          datasets: [{
            label: 'Track metrics',
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    }

    return null;
  }
}
