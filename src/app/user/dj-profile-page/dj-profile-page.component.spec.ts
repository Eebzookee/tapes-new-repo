import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DjProfilePageComponent } from './dj-profile-page.component';

describe('DjProfilePageComponent', () => {
  let component: DjProfilePageComponent;
  let fixture: ComponentFixture<DjProfilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DjProfilePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DjProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
