import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {Feedback} from '../../models/feedback/feedback.model';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../models/user/user.model';

@Component({
  selector: 'app-dj-profile-page',
  templateUrl: './dj-profile-page.component.html',
  styleUrls: ['./dj-profile-page.component.scss']
})
export class DjProfilePageComponent implements OnInit {

  feedback: Feedback[] = [];
  userId: number | null = null;
  user: User | null = null;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const param = this.route.snapshot.paramMap.get('userId');
    this.userId = param ? Number(param) : null;

    if (this.userId) {
      this.userService.getUser(this.userId).subscribe(response => {
        this.user = response;
      }, error => {
        console.log(error);
      });
    } else {
      this.user = this.userService.user.value;
    }

    this.userService.getMyFeedback(this.userId).subscribe(response => {
      this.feedback = response;
    }, error => {
      console.log(error);
    });
  }
}
