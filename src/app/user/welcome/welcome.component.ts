import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/navigation/header/header.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(private headerService: HeaderService,) { }

  ngOnInit(): void {
    this.headerService.changePageType('withmenu');
    this.headerService.changePageTitle('Welcome');    
  }

}
