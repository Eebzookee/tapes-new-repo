import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/models/playlist/playlist.model';
import { Track } from 'src/app/models/track/track.model';
import { AudioService } from 'src/app/services/audio/audio.service';
import { PlaylistService } from 'src/app/services/playlist/playlist.service';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  playlists: Playlist[] = [];

  // Create a playlist attributes
  likedTracks: Track[] = [];
  name = "";
  selectedTracks: Track[] = [];
  create = false;

  // View a specific playlist attributes
  selectedPlaylist: Playlist | null = null;

  constructor(
    private playlistService: PlaylistService,
    private audioService: AudioService
  ) { }

  ngOnInit(): void {
    this.fetchData();
  }

  fetchData() {
    this.playlistService.getMyPlaylists().subscribe(response => {
      this.playlists = response;
    }, err => {
      console.log(err);
    });

    this.audioService.getTracks({liked: true}).subscribe(response => {
      this.likedTracks = response;
    }, err => {
      console.log(err);
    });
  }

  showCreatePlaylist() {
    this.create = true;
  }

  selectTrack(track: Track) {
    this.selectedTracks.push(track);
    this.likedTracks.splice(this.likedTracks.indexOf(track), 1);
  }

  deselectTrack(track: Track) {
    this.likedTracks.push(track);
    this.selectedTracks.splice(this.selectedTracks.indexOf(track), 1);
  }

  savePlaylist() {
    this.playlistService.createPlaylist(this.name, this.selectedTracks.map(t => t.id)).subscribe(response => {
      this.create = false;
      this.fetchData();

      this.name = "";
      this.selectedTracks = [];
    }, err => {
      console.log(err)
    });
  }

  setSelectedPlaylist(playlist: Playlist) {
    this.selectedPlaylist = playlist;
  }

  goBack() {
    this.selectedPlaylist = null;
    this.create = false;
  }

  removeTrackFromPlaylist(track: Track) {
    return () => {
      if (this.selectedPlaylist) {
        this.selectedPlaylist.tracks.splice(this.selectedPlaylist.tracks.indexOf(track), 1);

        this.playlistService.removeTrackFromPlaylist(this.selectedPlaylist.id, track.id).subscribe(response => {
          console.log(response);
        }, err => {
          console.log(err);
        });
      }
    }
  }
}
