import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/navigation/header/header.service';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit {

  constructor(
    private headerService: HeaderService
  ) { }

  ngOnInit(): void {
    this.headerService.changePageTitle('My|Library');
  }

}
