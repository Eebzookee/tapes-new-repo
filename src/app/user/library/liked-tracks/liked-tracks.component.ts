import { Component, OnInit } from '@angular/core';
import { Track } from 'src/app/models/track/track.model';
import { AudioService } from 'src/app/services/audio/audio.service';

@Component({
  selector: 'app-liked-tracks',
  templateUrl: './liked-tracks.component.html',
  styleUrls: ['./liked-tracks.component.scss']
})
export class LikedTracksComponent implements OnInit {

  tracks: Track[] = [];

  constructor(
    private audioService: AudioService
  ) { }

  ngOnInit(): void {
    this.audioService.getTracks({liked: true}).subscribe(response => {
      this.tracks = response;
    }, err => {
      console.log(err);
    });
  }

}
