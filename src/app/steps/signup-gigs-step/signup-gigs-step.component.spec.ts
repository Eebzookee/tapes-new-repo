import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupGigsStepComponent } from './signup-gigs-step.component';

describe('SignupGigsStepComponent', () => {
  let component: SignupGigsStepComponent;
  let fixture: ComponentFixture<SignupGigsStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupGigsStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupGigsStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
