import { Component, Input, ViewChild, OnInit, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";

@Component({
  selector: "app-signup-gigs-step",
  templateUrl: "./signup-gigs-step.component.html",
  styleUrls: ["./signup-gigs-step.component.scss"],
})
export class SignupGigsStepComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  step = 0;
  addaGig: boolean = false;
  btnColour = "#242424";

  @ViewChild("radioFocous") rf!: ElementRef;
  @ViewChild("podcastFocous") pcf!: ElementRef;
  @ViewChild("clubFocous") cf!: ElementRef;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {}

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  public checkError = (controlName: string, errorName: string) => {
    return this.formGroup.controls[controlName].hasError(errorName);
  };

  addGig(el:any) {
    this.addaGig = true;
    setTimeout(() => {
      const  x = eval(el)
      x.nativeElement.focus();
    }, 0);
  }

  removeGig(i: number, gigtype: string) {
    const control = <FormArray>this.formGroup.controls[gigtype];
    control.removeAt(i);
  }

  enterGig(gigtext: string, gigtype: string) {
    const rs = this.formGroup.controls[gigtype] as FormArray;
    rs.push(
      this.formBuilder.group({ gig: this.formGroup.controls[gigtext].value })
    );
    this.addaGig = false;
    this.formGroup.patchValue({
      [gigtext]: "",
    });
    /*
    this.formGroup.patchValue({
      package: 'DJ',
    });
    */
    console.log('this.formGroup.controls', this.formGroup.controls)
  }

}
