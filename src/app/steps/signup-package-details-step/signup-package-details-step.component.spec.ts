import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupPackageDetailsStepComponent } from './signup-package-details-step.component';

describe('SignupPackageDetailsStepComponent', () => {
  let component: SignupPackageDetailsStepComponent;
  let fixture: ComponentFixture<SignupPackageDetailsStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupPackageDetailsStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupPackageDetailsStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
