import { Component, Input, ViewChild, OnInit, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { RevenueService } from 'src/app/services/revenue/revenue.service';
import { HeaderService } from "../../navigation/header/header.service";
import { UserService } from "../../services/user/user.service";
import { SignupRepository } from "../../services/signup/signup.repository";
import { Router } from '@angular/router';
import { handleError, PackageOptions } from "../../services/helpers";
import { Package } from 'src/app/models/package/package.model';
import { User } from "../../models/user/user.model";

@Component({
  selector: 'app-signup-package-details-step',
  templateUrl: './signup-package-details-step.component.html',
  styleUrls: ['./signup-package-details-step.component.scss']
})
export class SignupPackageDetailsStepComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  step = 0;
  pkg_highlight: any[] = [];
  packages: any[] = [];
  user: User | null = null;
  userId: number | null = null;
  

  constructor(
    private formBuilder: FormBuilder,
    private revenueService: RevenueService,
    private headerService: HeaderService,
    public userService: UserService,
    private signupRepository: SignupRepository,
    private router: Router,
    ) { }

  ngOnInit(): void {

    if (this.userId) {
      this.userService.getUser(this.userId).subscribe(
        (response) => {
          this.user = response;
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      this.user = this.userService.user.value!;
    }


    this.revenueService.getAllPackages().subscribe(response => {
      this.packages = response.Package;
      
      // sort this array according to the oridinal key
      this.packages.sort((a, b) => parseFloat(a.ordinal) - parseFloat(b.ordinal));

      this.packages.map((n,i) => {
        this.pkg_highlight.push(false)
      })

    }, error => {
      console.log(error);
    });


    this.formGroup = this.formBuilder.group({
      package_id: [""],
    });


  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }


  // this needs to be encoded in database  
  pickPackage(packageId:any) {
    this.formGroup.patchValue({
      package_id: packageId,
    });
    // pkg_highlight

    this.packages.map((n, i) => {
      this.pkg_highlight[i] = (this.formGroup.controls['package_id'].value === n.id) ? true : false;
    })

  }

  public checkError = (controlName: string, errorName: string) => {
    return this.formGroup.controls[controlName].hasError(errorName);
  };
  //package

  async orderNewCampaign() {
    this.headerService.changePageLoading("true");
    let formData = this.formGroup.value;
    formData["id"] = this.user!.id;

    this.revenueService.upgradePackage(formData).subscribe(response => {
      if (response.error) {
        console.log('upgrade up error')
        this.headerService.changePageLoading("false");
      } else {        
        setTimeout(() => {
          this.headerService.changePageLoading("false");
          this.router.navigateByUrl("/user/profile");
          /**
           * so....ordered new cammpaign this gives use 1 upload allowed alon with whatever uploads they've already had
           * 
           */
        }, 1000);
      }
    });
    
  }
}
