import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import { AudioService } from 'src/app/services/audio/audio.service';

@Component({
  selector: 'app-signup-user-details-step',
  templateUrl: './signup-user-details-step.component.html',
  styleUrls: ['./signup-user-details-step.component.scss']
})
export class SignupUserDetailsStepComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  @Input() barvalue!: number;
  fileToUpload: File | null = null;
  progress = 0;

  constructor(
    private audioService: AudioService,
  ) {
  }

  ngOnInit() {
    this.barvalue = 50;
    //this.audioRepository.setCount.subscribe((msg: any) => {
    //  this.barvalue = this.barvalue + parseInt(msg, 10);
    //})
  }

  public checkError = (controlName: string, errorName: string) => {
    if(controlName === 'passwordRepeat') {
      if(this.formGroup.controls['password'].value !== this.formGroup.controls['passwordRepeat'].value) {
        return { passwordValid : true }
      }
    }
    return this.formGroup.controls[controlName].hasError(errorName);
  }

  fileUploadOnChange($event: any) {
    this.fileToUpload = $event.target.files[0];
    if (this.fileToUpload) {
      this.audioService.uploadTrack(this.fileToUpload, progress => {
        this.progress = progress;
      });
    }
  }

  coverPhotoOnChange($event:any) {
    this.formGroup.patchValue({
      profilePicture: $event
    });
  }  

}
